package io.semantext.resource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.logmanager.Logger;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import io.semantext.core.domain.document.Document;
import io.semantext.core.domain.file.File;
import io.semantext.core.domain.text.Text;
import io.semantext.core.domain.text.TextList;
import io.semantext.core.service.DocumentAnalyzisService;

@Path("/text")
public class TextResource {

    Logger logger = Logger.getLogger("TextRessource");

    @Inject
    private DocumentAnalyzisService documentAnalyzisService;

    @Path("/upload")
    @POST
    //@Consumes(MediaType.APPLICATION_OCTET_STREAM)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response analyseDocument(@MultipartForm MultipartBody formData, @DefaultValue("none") @QueryParam("analyzer") String analyzer) throws Exception {

        if (formData.fileName == null || formData.fileName.isEmpty()) {
            return Response.status(Status.BAD_REQUEST).build();
        }

        //if (formData.mimeType == null || formData.mimeType.isEmpty()) {
        //    return Response.status(Status.BAD_REQUEST).build();
        //}
        Document document = new Document();

        
        File file = new File();
        document.setFile(file);

        file.setInputStream(formData.file);
        Object textList = null;
        if ("none".equals(analyzer)) {
            textList = (Object)documentAnalyzisService.extract(document);
        }    
        else {    
            textList = (Object)documentAnalyzisService.extractAndAnalyze(document, analyzer);
        } 
        if (textList != null) {
            return Response.ok().status(Status.CREATED).entity(textList).build();
        } else {
            return Response.serverError().build();
        }

    }

    @Path("/file")
    @GET
    //@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response analyseDocumentFile(@QueryParam("path") String filePath, @DefaultValue("none") @QueryParam("analyzer") String analyzer) throws Exception {

        java.io.File file =new java.io.File(filePath);
        logger.info("file:"+file.getAbsolutePath());
        Document document = new Document(file.getParentFile().getAbsolutePath(),file.getName());
        logger.info("document:"+document.toString());
        Object textList = null;
        if ("none".equals(analyzer)) {
            textList = (Object)documentAnalyzisService.extract(document);
        }    
        else {    
            textList = (Object)documentAnalyzisService.extractAndAnalyze(document, analyzer);
        }    
     
        if (textList != null) {
            logger.info("text:"+textList.toString());
            return Response.ok().status(Status.CREATED).entity(textList).build();
        } else {
            return Response.serverError().build();
        }

    }

    @Path("/dir")
    @GET
    //@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response analyseDocumentDir(@QueryParam("path") String dirPath, @DefaultValue("none") @QueryParam("analyzer") String analyzer) throws Exception {

        java.io.File file =new java.io.File(dirPath);

        Object textList = Arrays.asList(file.listFiles())
            .parallelStream()
            .map(
                 f-> "none".equals(analyzer)?
                    (Object)documentAnalyzisService.extract(
                         new Document(f.getParentFile().getAbsolutePath(),f.getName())):
                    (Object)documentAnalyzisService.extractAndAnalyze(
                    new Document(f.getParentFile().getAbsolutePath(),f.getName()),analyzer)
                    )
            .collect(Collectors.toList());
             
        if (textList != null) {
            return Response.ok().status(Status.CREATED).entity(textList).build();
        } else {
            return Response.serverError().build();
        }

    }

}