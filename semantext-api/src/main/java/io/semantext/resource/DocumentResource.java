package io.semantext.resource;

import java.util.Arrays;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.logmanager.Logger;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import io.semantext.core.domain.document.Document;
import io.semantext.core.domain.file.File;

import io.semantext.core.service.DocumentAnalyzisService;
import io.semantext.core.service.DocumentSearchEngineService;

@Path("/document")
public class DocumentResource {

    Logger logger = Logger.getLogger("DocumentRessource");

    @Inject
    private DocumentAnalyzisService documentAnalyzisService;

    
    @Inject
    private DocumentSearchEngineService documentSearchEngineService;

    @Path("/upload")
    @POST
    //@Consumes(MediaType.APPLICATION_OCTET_STREAM)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response analyseDocument(@MultipartForm MultipartBody formData) throws Exception {

        if (formData.fileName == null || formData.fileName.isEmpty()) {
            return Response.status(Status.BAD_REQUEST).build();
        }

        //if (formData.mimeType == null || formData.mimeType.isEmpty()) {
        //    return Response.status(Status.BAD_REQUEST).build();
        //}
        Document document = new Document();
        File file = new File();
        document.setFile(file);
       
        file.setInputStream(formData.file);
        Object output = (Object)documentAnalyzisService.content(document);
       
        if (output != null) {
            logger.info("document:"+output.toString());
            return Response.ok().status(Status.CREATED).entity(output).build();
        } else {
            return Response.serverError().build();
        }
    }

    @Path("/file")
    @GET
    //@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response analyseDocumentFile(@QueryParam("path") String filePath) throws Exception {

        java.io.File file =new java.io.File(filePath);
        logger.info("file:"+file.getAbsolutePath());
        Document document = new Document(file.getParentFile().getAbsolutePath(),file.getName());
        logger.info("document:"+document.toString());
        Object output = (Object)documentAnalyzisService.content(document);
       
        if (output != null) {
            logger.info("document:"+output.toString());
            return Response.ok().status(Status.CREATED).entity(output).build();
        } else {
            return Response.serverError().build();
        }

    }

    @Path("/metadata/file")
    @GET
    //@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response extractDocumentMetadataFile(@QueryParam("path") String filePath) throws Exception {

        java.io.File file =new java.io.File(filePath);
        logger.info("file:"+file.getAbsolutePath());
        Document document = new Document(file.getParentFile().getAbsolutePath(),file.getName());
        logger.info("document:"+document.toString());
        Object output = (Object)documentAnalyzisService.metadata(document);
       
        if (output != null) {
            logger.info("document:"+output.toString());
            return Response.ok().status(Status.CREATED).entity(output).build();
        } else {
            return Response.serverError().build();
        }

    }

    @Path("/index")
    @POST
    //@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addIndex(@QueryParam("path") String filePath) throws Exception {

        java.io.File file =new java.io.File(filePath);
        logger.info("file:"+file.getAbsolutePath());
        File domainFile = new File(file.getParentFile().getAbsolutePath(),file.getName());
        logger.info("file:"+domainFile.toString());
        documentSearchEngineService.addIndex(domainFile);
       
        
        return Response.ok().status(Status.CREATED).build();

    }

    @Path("/index")
    @GET
    //@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response readIndex(@QueryParam("from") Integer from, @QueryParam("to") Integer to) throws Exception {
        Object output = documentSearchEngineService.readIndex(from, to);
        logger.info("result:"+output);
        if (output != null) {
            return Response.ok().status(Status.FOUND).entity(output).build();
        } else {
            return Response.serverError().build();
        }

    }

    @Path("/dir")
    @GET
    //@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response analyseDocumentDir(@QueryParam("path") String dirPath) throws Exception {

        java.io.File file =new java.io.File(dirPath);

        Object output = Arrays.asList(file.listFiles())
            .parallelStream()
            .map(
                 f-> 
                    (Object)documentAnalyzisService.content(
                         new Document(f.getParentFile().getAbsolutePath(),f.getName()))
                   
                    )
            .collect(Collectors.toList());
             
        if (output != null) {
            return Response.ok().status(Status.CREATED).entity(output).build();
        } else {
            return Response.serverError().build();
        }

    }

    @Path("/metadata/dir")
    @GET
    //@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response extractDocumentMetadataDir(@QueryParam("path") String dirPath) throws Exception {

        java.io.File file =new java.io.File(dirPath);

        Object output = Arrays.asList(file.listFiles())
            .parallelStream()
            .map(
                 f-> 
                    (Object)documentAnalyzisService.metadata(
                         new Document(f.getParentFile().getAbsolutePath(),f.getName()))
                   
                    )
            .collect(Collectors.toList());
             
        if (output != null) {
            return Response.ok().status(Status.CREATED).entity(output).build();
        } else {
            return Response.serverError().build();
        }

    }

    @Path("/search")
    @GET
    //@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response indexDir(@QueryParam("q") String queryString) throws Exception {

        Object output = documentSearchEngineService.search(queryString);
       
        if (output != null) {
            return Response.ok().status(Status.CREATED).entity(output).build();
        } else {
            return Response.serverError().build();
        }


    }


}