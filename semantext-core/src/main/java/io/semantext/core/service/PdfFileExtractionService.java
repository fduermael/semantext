package io.semantext.core.service;

import javax.enterprise.context.ApplicationScoped;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.command.PipelineCommand;
import io.semantext.core.command.document.ExtractTextAndImageInPdfFileCommand;

import io.semantext.core.domain.document.Document;
import io.semantext.core.domain.file.File;


@ApplicationScoped
public class PdfFileExtractionService {

    Logger logger = LoggerFactory.getLogger("PdfFileExtractionService");


   

    public Document content(File file) {
        // File-Document
        PipelineCommand pipelineCommand = new PipelineCommand();
        
        pipelineCommand.add(new ExtractTextAndImageInPdfFileCommand()
            .inputMultiplicity(AbstractCommand.Multiplicity.one)
            .outputMultiplicity(AbstractCommand.Multiplicity.one));
        pipelineCommand.setInput(file);
        logger.info("before execute command");
        pipelineCommand.execute();
        logger.info("after execute command:");
        return (Document)pipelineCommand.getOutput();
    }    

}
