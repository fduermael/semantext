package io.semantext.core.service;

import java.util.List;
import java.util.Arrays;

import javax.enterprise.context.ApplicationScoped;


import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.command.PipelineCommand;
import io.semantext.core.command.document.ExtractDocumentMetadataCommand;
import io.semantext.core.command.document.ReadIndexDocumentCommand;
import io.semantext.core.command.file.SearchIndexDocumentCommand;
import io.semantext.core.command.system.AddIndexDocumentCommand;
import io.semantext.core.domain.document.Document;
import io.semantext.core.domain.file.File;


@ApplicationScoped
public class DocumentSearchEngineService {

    Logger logger = LoggerFactory.getLogger("DocumentSearchEngineService");


    public Void addIndex(File domainFile) {
        java.io.File file =new java.io.File(domainFile.getParentFileName(),domainFile.getName());
        Arrays.asList(file.listFiles()).forEach(
            f -> {
                if (f.isDirectory()) {
                    addIndex(new File(f.getParent(),f.getName()));
                }
                else { 
                      // File
                    PipelineCommand pipelineCommand = new PipelineCommand();
                    pipelineCommand.add(new ExtractDocumentMetadataCommand()
                    .inputMultiplicity(AbstractCommand.Multiplicity.one)
                    .outputMultiplicity(AbstractCommand.Multiplicity.one));
                    pipelineCommand.add(new AddIndexDocumentCommand()
                    .inputMultiplicity(AbstractCommand.Multiplicity.one)
                    .outputMultiplicity(AbstractCommand.Multiplicity.one));
                    Document d = new Document(f.getParent(), f.getName());
                    pipelineCommand.setInput(d);
                    pipelineCommand.execute();
                    logger.info(f.getAbsolutePath()+" indexed.");
          
                }    
        });
      return null;
    }

    public List<Document> readIndex(Integer from, Integer to) {
        PipelineCommand pipelineCommand = new PipelineCommand();

        pipelineCommand.add(new ReadIndexDocumentCommand()
                .inputMultiplicity(AbstractCommand.Multiplicity.one)
                .outputMultiplicity(AbstractCommand.Multiplicity.many))
                .parameter("from",from)
                .parameter("to", to);

        pipelineCommand.setInput(null);
        pipelineCommand.execute();
        return (List)pipelineCommand.getOutputList();
    }


    public List<File> search(String queryString) {
        // File-Document
        PipelineCommand pipelineCommand = new PipelineCommand();
        pipelineCommand.add(new SearchIndexDocumentCommand()
            .inputMultiplicity(AbstractCommand.Multiplicity.one)
            .outputMultiplicity(AbstractCommand.Multiplicity.many));
        pipelineCommand.setInput(queryString);
        logger.info("before execute command");
        pipelineCommand.execute();
        logger.info("after execute command:");
        return (List<File>)pipelineCommand.getOutput();
    }        


  
}


