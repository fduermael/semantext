package io.semantext.core.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.command.PipelineCommand;
import io.semantext.core.command.document.ExtractDocumentMetadataAndContentCommand;
import io.semantext.core.command.document.ExtractDocumentMetadataCommand;
import io.semantext.core.command.text.SpacyAnalyzeTextCommand;
import io.semantext.core.command.text.Analyzer;
import io.semantext.core.command.text.ExtractParagraphInDocumentCommand;
import io.semantext.core.command.text.SpacyJaxRSWebAnalyzeTextCommand;
import io.semantext.core.command.text.SpacyRestAnalyzeTextCommand;
import io.semantext.core.command.text.StanfordCoreNLPAnalyzeTextCommand;
import io.semantext.core.domain.document.Document;
import io.semantext.core.domain.file.File;
import io.semantext.core.domain.text.Text;

@ApplicationScoped
public class DocumentAnalyzisService {

    Logger logger = LoggerFactory.getLogger("DocumentAnalyzisService");


    public Document metadata(Document document) {
        // File
        PipelineCommand pipelineCommand = new PipelineCommand();
        pipelineCommand.add(new ExtractDocumentMetadataCommand()
        .inputMultiplicity(AbstractCommand.Multiplicity.one)
        .outputMultiplicity(AbstractCommand.Multiplicity.one));
        pipelineCommand.setInput(document);
        logger.info("before execute command");
        pipelineCommand.execute();
        logger.info("after execute command:");
        return (Document)pipelineCommand.getOutput();
    }        

    public Document content(Document document) {
        // File-Document
        PipelineCommand pipelineCommand = new PipelineCommand();
        /*
        pipelineCommand.add(new ExtractDocumentMetadataCommand()
        .inputMultiplicity(AbstractCommand.Multiplicity.one)
        .outputMultiplicity(AbstractCommand.Multiplicity.one));
        */
        pipelineCommand.add(new ExtractDocumentMetadataAndContentCommand()
            .inputMultiplicity(AbstractCommand.Multiplicity.one)
            .outputMultiplicity(AbstractCommand.Multiplicity.one));
        pipelineCommand.setInput(document);
        logger.info("before execute command");
        pipelineCommand.execute();
        logger.info("after execute command:");
        return (Document)pipelineCommand.getOutput();
    }        


    public List<Text> extract(Document document) {
        // Document-Document<Text
        PipelineCommand pipelineCommand = new PipelineCommand();
        pipelineCommand.add(new ExtractDocumentMetadataAndContentCommand()
            .inputMultiplicity(AbstractCommand.Multiplicity.one)
            .outputMultiplicity(AbstractCommand.Multiplicity.one));
        pipelineCommand.add(new ExtractParagraphInDocumentCommand()
            .inputMultiplicity(AbstractCommand.Multiplicity.one)
            .outputMultiplicity(AbstractCommand.Multiplicity.many));
        pipelineCommand.setInput(document);
        logger.info("before execute command");
        pipelineCommand.execute();
        logger.info("after execute command:");
        return (List<Text>)pipelineCommand.getOutput();
    }        

    public List<Text> extractAndAnalyze(Document document, String analyzer) {
        // Document-Document<Text=Text
        PipelineCommand pipelineCommand = new PipelineCommand();
        pipelineCommand.add(new ExtractDocumentMetadataAndContentCommand()
            .inputMultiplicity(AbstractCommand.Multiplicity.one)
            .outputMultiplicity(AbstractCommand.Multiplicity.one));
        pipelineCommand.add(new ExtractParagraphInDocumentCommand()
            .inputMultiplicity(AbstractCommand.Multiplicity.one)
            .outputMultiplicity(AbstractCommand.Multiplicity.many));
    
        //pipelineCommand.add(new ExtractTextInPdfFileCommand()
        //    .inputMultiplicity(AbstractCommand.Multiplicity.one)
        //    .outputMultiplicity(AbstractCommand.Multiplicity.one));
        if(Analyzer.stanford_corenlp.name().equals(analyzer)) {
            pipelineCommand.add(new StanfordCoreNLPAnalyzeTextCommand()
                .inputMultiplicity(AbstractCommand.Multiplicity.many)
                .outputMultiplicity(AbstractCommand.Multiplicity.many));
        }     
        else if(Analyzer.spacy.name().equals(analyzer)) {
            pipelineCommand.add(new SpacyAnalyzeTextCommand()
                .inputMultiplicity(AbstractCommand.Multiplicity.many)
                .outputMultiplicity(AbstractCommand.Multiplicity.many));
        }  
        else if(Analyzer.spacy_restclient.name().equals(analyzer)) {
            pipelineCommand.add(new SpacyRestAnalyzeTextCommand()
                .inputMultiplicity(AbstractCommand.Multiplicity.many)
                .outputMultiplicity(AbstractCommand.Multiplicity.many));
        }
        else if(Analyzer.spacy_jaxrswebclient.name().equals(analyzer)) {
            pipelineCommand.add(new SpacyJaxRSWebAnalyzeTextCommand()
                .inputMultiplicity(AbstractCommand.Multiplicity.many)
                .outputMultiplicity(AbstractCommand.Multiplicity.many));
        }    
        else if(Analyzer.spacy_resteasyclient.name().equals(analyzer)) {
            pipelineCommand.add(new SpacyJaxRSWebAnalyzeTextCommand()
                .inputMultiplicity(AbstractCommand.Multiplicity.many)
                .outputMultiplicity(AbstractCommand.Multiplicity.many));
        }    
        pipelineCommand.setInput(document);
        logger.info("before execute command");
        pipelineCommand.execute();
        logger.info("after execute command:");
        return (List<Text>)pipelineCommand.getOutput();
    }


}
