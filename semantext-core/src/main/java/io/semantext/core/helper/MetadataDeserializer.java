package io.semantext.core.helper;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import io.semantext.core.domain.base.Metadata;

public class MetadataDeserializer extends StdDeserializer<Metadata> { 

    public MetadataDeserializer() { 
        this(null); 
    } 

    public MetadataDeserializer(Class<?> vc) { 
        super(vc); 
    }

    @Override
    public Metadata deserialize(JsonParser jp, DeserializationContext ctxt) 
      throws IOException, JsonProcessingException {
         
        JsonNode jsonNode = jp.getCodec().readTree(jp);
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(jsonNode, new TypeReference<Map<String, Object>>(){});    
        
        return new Metadata(map);
    }
}