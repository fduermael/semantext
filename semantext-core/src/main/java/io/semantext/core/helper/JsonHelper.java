package io.semantext.core.helper;

import java.util.Base64;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import io.semantext.core.domain.base.Base;
import io.semantext.core.domain.base.Metadata;

public class JsonHelper {


    private static ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
      
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Metadata.class, new MetadataDeserializer());
        objectMapper.registerModule(module);
        
    }    

    public static ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public static String serializeJson(Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        }
        catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String serializeJsonBase64(Base obj) {
        String serializedInputText = serializeJson(obj);
        return Base64.getEncoder().encodeToString(
            serializedInputText.getBytes());

    }        

    public static Base deserializeJson(String s, Class clazz) {
        try {
            return (Base) objectMapper.readValue(s, clazz);
        }
        catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}

