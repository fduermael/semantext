package io.semantext.core.helper;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.text.PDFTextStripper;

import java.awt.image.RenderedImage;
import java.io.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class PdfBoxHelper {

    public static List<RenderedImage> getImagesFromResources(PDResources resources)  {
        List<RenderedImage> images = new ArrayList<>();

        for (COSName xObjectName : resources.getXObjectNames()) {
            try {
                PDXObject xObject = resources.getXObject(xObjectName);
                        if (xObject instanceof PDFormXObject) {
                images.addAll(getImagesFromResources(((PDFormXObject) xObject).getResources()));
            } else if (xObject instanceof PDImageXObject) {
                images.add(((PDImageXObject) xObject).getImage());
            }
        }
        catch (IOException e) {
            continue;
        }


        }

        return images;
    }

    static void extractAllTextsAndImages(InputStream inputStream, String path) throws IOException {
        PDDocument document = PDDocument.load(inputStream);
        Integer numberOfPages = document.getNumberOfPages();
        new File(path).mkdirs();
        for (int i = 1; i <= numberOfPages; i++) {
            String folderName = path + "/" + "page" + i;
            new File(folderName).mkdirs();

            PDFTextStripper stripper = new PDFTextStripper();
            stripper.setStartPage(i);
            stripper.setEndPage(i);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            Writer writer = new PrintWriter(byteArrayOutputStream);
            stripper.writeText(document, writer);
            writer.close();
            File textFile = new File(folderName, "content.txt");
            try (OutputStream outputStream = new FileOutputStream(textFile)) {
                outputStream.write(byteArrayOutputStream.toByteArray());
            }


            PDPage page = document.getPages().get(i-1);
            List<RenderedImage> renderedImageList = getImagesFromResources(page.getResources());
            List<byte[]> imageByteArrayList = renderedImageList.stream().map(ImageHelper::toByteArray).collect(Collectors.toList());
            for (int j = 1; j <= imageByteArrayList.size(); j++) {
                File imageFile = new File(folderName, "image" + j + ".png");
                try (OutputStream outputStream = new FileOutputStream(imageFile)) {
                    outputStream.write(imageByteArrayList.get(j - 1));
                }
            }
        }

    }

    
}
