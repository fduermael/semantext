package io.semantext.core.helper;

import org.eclipse.microprofile.config.ConfigProvider;

public class ConfigHelper {

    static public String getStringValue(String name) {
        return ConfigProvider.getConfig().getValue(name, String.class);
    }

    
}
