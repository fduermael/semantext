package io.semantext.core.factory.text;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class StanfordCoreNLPAnalyzerFactory {
    

    private static Map<String,String> availableLanguageMap;

    
    private static Map<String,StanfordCoreNLP> instanceMap = new HashMap<>();
    
    static {
        availableLanguageMap = new HashMap<>();
        availableLanguageMap.put("fr","french");
        availableLanguageMap.put("en","english");
    }
   
    public static void initAll() {
        availableLanguageMap.entrySet().forEach(
            entry -> instanceMap.put(entry.getKey(),new StanfordCoreNLP(entry.getValue()))
        );

        
    }

    public static StanfordCoreNLP getInstance(String locale) {
        if (locale == null) {
            // default locale
            locale= Locale.ENGLISH.getLanguage();
        }
        if (availableLanguageMap.get(locale) == null ) {
            return null;
        }
        StanfordCoreNLP instance = instanceMap.get(locale);
        if (instanceMap.get(locale)==null) {
            if (Locale.ENGLISH.getLanguage().equals(locale)) {
                Properties props = new Properties();
                props.setProperty("annotators", "tokenize,ssplit,pos,lemma,depparse,natlog,openie");
                props.setProperty("tokenize.language","en");
                props.setProperty("pos.model","edu/stanford/nlp/models/pos-tagger/english-left3words-distsim.tagger");
                props.setProperty("depparse.model","edu/stanford/nlp/models/parser/nndep/english_UD.gz");

                instance = new StanfordCoreNLP(props);
            }
            else {
                instance = new StanfordCoreNLP(availableLanguageMap.get(locale));
            }
            instanceMap.put (locale,instance);
        } 
       
        return instance;
    }

    public static StanfordCoreNLP getInstance(Locale locale) {
        return getInstance(locale.getLanguage());
    }
}
