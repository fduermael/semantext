package io.semantext.core.factory.text;

import org.apache.tika.Tika;

/**
 * Tika singleton factory
 */
public class TikaFactory {

    private static Tika instance = new Tika();
    
    public static Tika getInstance() {
        return instance;
    }

}