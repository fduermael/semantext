package io.semantext.core.domain.base;

public interface TreeNodeVisitor {

    void visit(TreeNode<? extends Base> treeNode);
    
}
