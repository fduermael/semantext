package io.semantext.core.domain.text;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
public enum TextMetadataKey {

    lang,
    
    pos,

    tag,

    aspect,

    @JsonProperty("case")
    caze,

    form,

    gender,

    mood,

    number,

    person,

    proper,

    reciprocity,

    tense,

    voice,

    beginDate,
    
    endDate,

    sem_relation,

    index,

    head,

    head_score,

    head_relation,

    head_index

}
