package io.semantext.core.domain.text;

import io.semantext.core.domain.base.Base;
import io.semantext.core.domain.document.Section;
import io.semantext.core.domain.base.TreeNode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * name relevant when token (lemma) or morpheme
 */
public class Text extends Base {
    
    /**
     * raw form without treatment (not modifiable)
     */
    private String raw;


    /**
     * null if root
     */
    @JsonIgnore
    private Section container = null;

    /**
     * offset in container
     */
    private Integer offset = null;

    /**
     * length in container
     */
    private Integer length = null;


    /**
     * flat composition according to sequence order
     * for instance : J,''',ai mangé,hier,des,pommes de terre,sautées
     */
    private List<Text> componentList = new ArrayList<>();


    public Text() {
        super();
    }

    public Text(Section container, String raw) {
        this.container = container;
        this.raw = raw;
        if (container != null && container.getText()!=null) {
            setLang(container.getText().getLang());
        }
    }

    public Text(Section container, Integer offset, Integer length) {
        this.container = container;
        this.raw = null;
        this.offset = offset;
        this.length = length;
        if (container != null && container.getText()!=null) {
            setLang(container.getText().getLang());
        }    
    }

    

    public Text(String lang, String raw) {
        setLang(lang);
        this.raw = raw;
    }

    public Text(Locale locale, String raw) {
        this.raw = raw;
        setLang(locale.getLanguage());
    }

    public Text(String raw) {
        this.raw = raw;
    }

    public Integer getOffset() {
        return offset;
    }

    public Integer getLength() {
        return length;
    }

    
    public String getRaw() {
        if (raw != null) {
            return raw;
        }
        else if (container != null && container.getText()!= null) {
            String containerRaw = container.getText().getRaw();
            if (containerRaw != null) {
                if (offset != null) {
                    if (length != null) {
                        return containerRaw.substring(offset,offset+length);
                    }   
                    return containerRaw.substring(offset); 
                }
                //return containerRow
            }
        }
        return null;
    }

    public String getName() {
        if (super.getName() != null) {
            return super.getName();
        }
        return getRaw();
    }    

    public String getLang() {
        return  (String)getMetadata().get(TextMetadataKey.lang.name());
    }

    public void setLang(String lang) {
        getMetadata().put(TextMetadataKey.lang.name(),lang);;
    }

    public Section getContainer() {
        return container;
    }

    public List<Text> getComponentList() {
        return componentList;
    }

    public void setComponentList(List<Text> componentList) {
        this.componentList = componentList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Text that = (Text) o;
        if (getLang() == null) {
            return super.getName().equals(that.getName());
        } 
        return super.getName().equals(that.getName()) &&
                getLang().equals(that.getLang());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.getName(), getLang());
    }

    @Override
    public String toString() {
        return "Text [" + super.toString() + " lang=" + getLang() + ", raw=" + raw + ", componentList="+ componentList.toString() + "]";
    }


    /**
     * collects all token index  of the component list
     * @return
     */

    public Set<Integer> makeTokenIndexSet() {
        return getComponentList()
            .stream()
            .map(text -> (Integer)text.getMetadata().get("index"))
            .collect(Collectors.toSet());
    
    }

   
    /**
     * map token index to index in componentList
     * @param offset
     * @return
     */
    public Map<Integer, Integer> makeIndexMap(Integer offset) {
        Map<Integer, Integer> map = new HashMap<>();
        int listIndex = offset;
        for (Integer tokenIndexList : makeTokenIndexSet()) {
            map.put(tokenIndexList, listIndex);
            listIndex++;
        }
        return map;    
    }


    /**
     * get all roots for instance 
     * path = "head_relation"
     * value = "ROOT"
     */
    public List<Text> findComponentsByMetadataPath(String path, Object value) {
        return getComponentList().stream().filter(
           text -> value.equals(text.getMetadata().get(path))
        ).collect(Collectors.toList());
    }    
    

    public List<TreeNode<Text>> makeDependencyTreeList() {
        return findComponentsByMetadataPath("dep_head_relation", "ROOT")
            .stream()
            .map (root-> buildDependencyTree(makeTokenIndexSet(), new TreeNode<Text>(root)))
            .collect(Collectors.toList());

    }

   /**
    * recursive method to build dependencyTree
     each indexMap is modified until it has no more keys
    * @param indexMap
    * @param head
    */ 
   private TreeNode<Text> buildDependencyTree(Set<Integer> tokenIndexSet, TreeNode<Text> head) {

        Integer headToCompareTokenIndex = (Integer)head.getValue().getMetadata().get("index");
        
        // iterate through each token (word) of the components (of the sentence)
        getComponentList().forEach( 
            text -> {      
                Integer headTextTokenIndex = (Integer)text.getMetadata().get("dep/head/index");

                if (tokenIndexSet.contains(headTextTokenIndex)) {
                    Integer textTokenIndex = (Integer)text.getMetadata().get("index");
                    // true not reflexive  dependency-head relationship
                    if (headToCompareTokenIndex.equals(headTextTokenIndex)&& !(headTextTokenIndex.equals(textTokenIndex))) {                        
                        //System.out.println(text.getName());
                        TreeNode<Text> textTreeNode = new TreeNode<Text>();
                        textTreeNode.setLevel(head.getLevel()+1);
                        textTreeNode.setHead(head);
                        Object headRelation = text.getMetadata().get("dep/head/relation");
                        if (headRelation != null) {
                            textTreeNode.setHeadRelation(headRelation.toString());
                        }
                        textTreeNode.setValue(text);
                        head.getDependencyList().add(buildDependencyTree(tokenIndexSet, textTreeNode));                        
                    }
                }
            }
        );
        // to more to be considered
        tokenIndexSet.remove(headToCompareTokenIndex);   
        return head;
   }

}
