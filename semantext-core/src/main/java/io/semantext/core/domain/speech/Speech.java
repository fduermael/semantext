package io.semantext.core.domain.speech;

import io.semantext.core.domain.base.Base;

public class Speech extends Base {

    private byte[] raw;

    public byte[] getRaw() {
        return raw;
    }

    public void setRaw(byte[] raw) {
        this.raw = raw;
    }

}
