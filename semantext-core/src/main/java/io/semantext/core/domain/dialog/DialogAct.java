package io.semantext.core.domain.dialog;

import io.semantext.core.domain.entity.Entity;
import io.semantext.core.domain.text.Text;

public class DialogAct {

    private String locutor;

    private Text utterance;

    private Entity intent;

    public String getLocutor() {
        return locutor;
    }

    public void setLocutor(String locutor) {
        this.locutor = locutor;
    }

    public Text getUtterance() {
        return utterance;
    }

    public void setUtterance(Text utterance) {
        this.utterance = utterance;
    }

    public Entity getIntent() {
        return intent;
    }

    public void setIntent(Entity intent) {
        this.intent = intent;
    }

    
    
}
