package io.semantext.core.domain.document;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.semantext.core.domain.base.Base;
import io.semantext.core.domain.document.Document;
import io.semantext.core.domain.image.Image;
import io.semantext.core.domain.text.FramedText;


/** 
 * componentList is used to divide  text with components, offset, length relative to this container
 * type (inherited) can be a volume, part, chapter, paragraph (structured content oriented), or page (structured form oriented)
 * 
 */
public class Page extends Base {

    public Page(Document container) {
        container.getPageList().add(this);
    }

    @JsonIgnore
    private List<Image> imageList = new ArrayList<>();

    private List<FramedText> framedTextList = new ArrayList<>();

    
    public List<FramedText> getFramedTextList() {
        return framedTextList;
    }

    public void setFramedTextList(List<FramedText> framedTextList) {
        this.framedTextList = framedTextList;
    }

    public List<Image> getImageList() {
        return imageList;
    }

    public void setImageList(List<Image> imageList) {
        this.imageList = imageList;
    }

    
}
