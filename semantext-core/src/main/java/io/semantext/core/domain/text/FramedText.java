package io.semantext.core.domain.text;

import io.semantext.core.domain.space.Position;

public class FramedText {

    private Text text;

    private Position position;

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    
}
