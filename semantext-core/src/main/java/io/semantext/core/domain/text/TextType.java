package io.semantext.core.domain.text;

public enum TextType {
    
    paragraph,
    sentence,
    clause,
    group,
    phrase,
    lexeme,
    morpheme,
    token

}
