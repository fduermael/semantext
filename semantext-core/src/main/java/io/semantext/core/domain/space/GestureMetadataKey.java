package io.semantext.core.domain.space;

public enum GestureMetadataKey {
    beginDate,
    endDate
}
