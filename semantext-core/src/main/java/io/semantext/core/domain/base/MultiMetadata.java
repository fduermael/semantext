package io.semantext.core.domain.base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * use composite keys (path with separators) to access information in metadata
 */
public class MultiMetadata extends HashMap<String, Object> {
    
    /**
     * single of multivalue valueMode
     */
    public enum ValueMode {
        single, 
        multiple
    }

    public class ValueModeList extends ArrayList<Object> {

        @JsonIgnore
        private ValueMode valueMode = ValueMode.single;

        public ValueMode getValueMode() {
            return valueMode;
        }
    
        public void setValueMode(ValueMode valueMode) {
            this.valueMode = valueMode;
        }
    }

    public static String separator = "/";
    public static String separatorRegex = "/";

    @JsonIgnore
    private ValueMode defaultValueMode = ValueMode.single;

     public MultiMetadata() {
        super();
    }    

    public MultiMetadata(Map map) {
        super(map);
        //init();
      
    } 
    
    public void init() {
        // init with composite key
        
        for (String key : super.keySet()) {
            put(key, super.get(key));
        }
    }


    /**
     * in single valueMode, overwrite values
     */
    public Object put(String compositeKey, Object value) {
        List<String> keyList = split(compositeKey);
        if (keyList.size()==1) {
            String key = keyList.get(0);
            Object terminalValue = super.get(key);
            boolean created = false;
            if ((terminalValue == null)) {
                terminalValue = new ValueModeList();   
                ((ValueModeList)terminalValue).setValueMode(defaultValueMode); 
                created = true;
            }
            if (terminalValue instanceof ValueModeList) {
                if (!created) {
                    ValueMode oldValueMode = ((ValueModeList)terminalValue).getValueMode();
                    if (oldValueMode.equals(ValueMode.single)) {
                        //reset
                        terminalValue = new ValueModeList();   
                        ((ValueModeList)terminalValue).setValueMode(oldValueMode);
                    }    
                }
                ((ValueModeList)terminalValue).add(value);                
            }   
             
            return getValue(super.put(key,terminalValue));    
        } 
        else if (keyList.size()>1) {
            String firstKey = keyList.get(0);
            Object intermediateValue = super.get(firstKey);
            
            if (!(intermediateValue instanceof Metadata)) {
                intermediateValue = new Metadata();
                ((MultiMetadata)intermediateValue).setDefaultValueMode(defaultValueMode);
                super.put(firstKey,intermediateValue);
            }
            List<String> newKeyList = new ArrayList<>();
            for (int i=1; i<keyList.size(); i++) {
                newKeyList.add(keyList.get(i));
            }

            return ((MultiMetadata)intermediateValue).put(join(newKeyList), value);
        }
        else {
            return null;
        }
    }
    /**
     * 
     * @param compositeKey
     * @return
     */

    public Object get(String compositeKey) {
        List<String> keyList = split(compositeKey);
        if (keyList.size()==1) {
            Object terminalValue = super.get(keyList.get(0));
            return getValue(terminalValue);
        } 
        else if (keyList.size()>1) {
            String firstKey = keyList.get(0);
            Object intermediateValue = super.get(firstKey);
            if (intermediateValue instanceof Metadata) {
                List<String> newKeyList = new ArrayList<>();
                for (int i=1; i<keyList.size(); i++) {
                    newKeyList.add(keyList.get(i));
                }
                return ((MultiMetadata)intermediateValue).get(join(newKeyList));
            } 
            else if (intermediateValue instanceof Map) {
                List<String> newKeyList = new ArrayList<>();
                for (int i=1; i<keyList.size(); i++) {
                    newKeyList.add(keyList.get(i));
                }
                MultiMetadata newIntermediateValue = new MultiMetadata((Map)intermediateValue);
                super.put(firstKey, newIntermediateValue);
                return newIntermediateValue.get(join(newKeyList));
            } 
            else {    
                return getValue(intermediateValue);
            }    
        }
        else {
            return null;
        }
    }

/**
     * in single valueMode, overwrite values
     */
    public void register(String compositeKey, ValueMode valueMode) {
        List<String> keyList = split(compositeKey);
        if (keyList.size()==1) {
            String key = keyList.get(0);
            Object terminalValue = super.get(key);
            if (terminalValue == null) {
                terminalValue = new ValueModeList();
                super.put(key, terminalValue);
            }    
            if (terminalValue instanceof ValueModeList) {
                ((ValueModeList)terminalValue).setValueMode(valueMode);                
            }   
            
           
        } 
        else if (keyList.size()>1) {
            String firstKey = keyList.get(0);
            Object intermediateValue = super.get(firstKey);
            
            if (!(intermediateValue instanceof Metadata)) {
                intermediateValue = new Metadata();
                ((MultiMetadata)intermediateValue).setDefaultValueMode(defaultValueMode);
                super.put(firstKey,intermediateValue);
            }
            List<String> newKeyList = new ArrayList<>();
            for (int i=1; i<keyList.size(); i++) {
                newKeyList.add(keyList.get(i));
            }

            ((MultiMetadata)intermediateValue).register(join(newKeyList), valueMode);
        }
   
    }


    /**
     * 
     * @param compositeKey
     * @return
     */

    public ValueMode getValueMode(String compositeKey) {
        List<String> keyList = split(compositeKey);
        if (keyList.size()==1) {
            Object terminalValue = super.get(keyList.get(0));
            if ((terminalValue != null) && (terminalValue instanceof ValueModeList)&&((ValueModeList)terminalValue).getValueMode().equals(ValueMode.multiple)) {
                ((ValueModeList)terminalValue).getValueMode();                
            }  
            return null; 
        } 
        else if (keyList.size()>1) {
            String firstKey = keyList.get(0);
            Object intermediateValue = super.get(firstKey);
            if (intermediateValue instanceof Metadata) {
                List<String> newKeyList = new ArrayList<>();
                for (int i=1; i<keyList.size(); i++) {
                    newKeyList.add(keyList.get(i));
                }
                return ((MultiMetadata)intermediateValue).getValueMode(join(newKeyList));
            }
            else if (intermediateValue instanceof ValueModeList) {
    
                return((ValueModeList)intermediateValue).getValueMode();
            }    
            return null;
        }
        else {
            return null;
        }
    }

    /**
     * value of a terminal 
     * @param value
     * @return
     */
    private Object getValue(Object value) {
        if (value instanceof ValueModeList && ((ValueModeList)value).getValueMode().equals(ValueMode.single)) {    
            if (((ValueModeList)value).isEmpty()) {
                return null;
            }     
            else {
                return ((ValueModeList)value).get(0);
            }
         }    
         return value;
    }


    public ValueMode getDefaultValueMode() {
        return defaultValueMode;
    }

    public void setDefaultValueMode(ValueMode valueMode) {
        this.defaultValueMode = valueMode;
        // propagate valueMode
        
        values().forEach(
            value -> {
                if (value instanceof Metadata) {
                    ((MultiMetadata)value).setDefaultValueMode(valueMode);
                }        
            }
        );
        

    }



    private List<String> split(String compositeKey) {
        List<String> keyList = Arrays.asList(compositeKey.split(separatorRegex));
        return keyList;
    }

    private String join(List<String> keyList) {
        return String.join(separator, keyList);
    }

}
