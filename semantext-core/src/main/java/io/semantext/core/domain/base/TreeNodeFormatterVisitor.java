package io.semantext.core.domain.base;

import java.io.PrintWriter;
import java.nio.CharBuffer;

public class TreeNodeFormatterVisitor implements TreeNodeVisitor {

    
    private PrintWriter printWriter;


    public TreeNodeFormatterVisitor() {
        this.printWriter = new PrintWriter(System.out);
    }


    public TreeNodeFormatterVisitor(PrintWriter out) {
        this.printWriter = out;
    }


    public PrintWriter getPrintWriter() {
        return printWriter;
    }

    public void setPrintWriter(PrintWriter printWriter) {
        this.printWriter = printWriter;
    }

    public void visit(TreeNode<? extends Base> treeNode)  {
        printWriter.print(spaces(2*treeNode.getLevel()));
        printWriter.print("[");
        printWriter.print(treeNode.getHeadRelation());
        printWriter.print("] ");
        printWriter.print(treeNode.getValue().getName());

        Object pos = treeNode.getValue().getMetadata().get("pos");
        if (pos != null)  {
            printWriter.print(" (");
            printWriter.print(pos);
            printWriter.print(")");
        }    
        Object feat = treeNode.getValue().getMetadata().get("feat");
        if (feat != null) {
            printWriter.print(" ");
            printWriter.println(feat) ;
        } 
        else {
            printWriter.println();
        }  
        //propagate to dependency
        treeNode.getDependencyList().forEach(
            dependency -> {
                dependency.accept(this);
            }); 
        printWriter.flush();
    }

    /**
      * Creates a string of spaces that is 'spaces' spaces long.
      *
      * @param spaces The number of spaces to add to the string.
    */
    private String spaces( int spaces ) {
        return CharBuffer.allocate( spaces ).toString().replace( '\0', ' ' );
    }

}
