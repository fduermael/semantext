package io.semantext.core.domain.document;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.semantext.core.domain.image.Image;
import io.semantext.core.domain.text.Text;


/** 
 * componentList is used to divide  text with components, offset, length relative to this container
 * type (inherited) can be a volume, part, chapter, paragraph (structured content oriented), or page (structured form oriented)
 * 
 */
public class Section extends Document {


    @JsonIgnore
    private List<Image> imageList = new ArrayList<>();

    private Text text;

    // container is either the document itself or a more higher level section
    public Section(Document container) {
        container.getSectionList().add(this);
    }

    public Section(Document container, String raw) {
        text = new Text(raw);
        container.getSectionList().add(this);
    }

    public Section(Document container, Text text) {
        this.text = text;
        container.getSectionList().add(this);
    }

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }
    
    public List<Image> getImageList() {
        return imageList;
    }

    public void setImageList(List<Image> imageList) {
        this.imageList = imageList;
    }

    
}
