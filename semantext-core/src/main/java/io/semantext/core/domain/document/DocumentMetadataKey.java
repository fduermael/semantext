package io.semantext.core.domain.document;

public enum DocumentMetadataKey {
    author,
    publishDate,
}
