package io.semantext.core.domain.document;

import java.util.ArrayList;
import java.util.List;

import io.semantext.core.domain.file.File;
import io.semantext.core.domain.note.NoteCollectionMap;
import io.semantext.core.domain.base.Base;

/**
 * document type is MIME type (application/pdf for instance)
 * Two views : 
 * - flat surface view (page list)
 * - deep structured view (recursive section view) 
 * To convert from syrface to dep view while avoiding duplicated text, use paragraph section level
 */
public class Document extends Base {


    public Document() {
        super();
    }

    public Document(String parent, String name) {
        file = new File(parent,name);
    }

    //TODO : make a factory
    public Document(NoteCollectionMap nodeCollectionMap) {
        nodeCollectionMap.forEach((label,noteList)-> {
           Section labelSection = new Section(this);
           labelSection.getMetadata().put("dc:title",labelSection);
           noteList.forEach(note->{
                Section noteSection = new Section(labelSection, note.getText());
                noteSection.getMetadata().put("type","note");
                noteSection.getMetadata().put("dc:title",note.getTitle());
           });
           getSectionList().add(labelSection);
        });
    }

    private File file;

    private List<Section> sectionList = new ArrayList<>();

    private List<Page> pageList = new ArrayList<>();

    public List<Page> getPageList() {
        return pageList;
    }

    public void setPageList(List<Page> pageList) {
        this.pageList = pageList;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public List<Section> getSectionList() {
        return sectionList;
    }

    public void setSectionList(List<Section> sectionList) {
        this.sectionList = sectionList;
    }


    
}
