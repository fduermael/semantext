package io.semantext.core.domain.entity;

import io.semantext.core.domain.text.TextSet;
import io.semantext.core.factory.entity.EntityFactory;
import io.semantext.core.service.OntologyService;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLEntity;

import java.util.Locale;

public abstract class NamedEntity<T extends OWLEntity> extends Entity<T> {

    /**
     * null if anonymous
     */
    protected TextSet name;
 

    public TextSet getName() {
        return name;
    }

    public void setName(TextSet name) {
        this.name = name;
    }

    public NamedEntity<T> name(String localizedName, String language) {
        TextSet name = getName();
        if (name == null) {
            name = new TextSet();
        }
        name.add(language, localizedName);

        OWLAnnotation commentAnnotation = EntityFactory.getInstance().getDataFactory()
                .getOWLAnnotation(EntityFactory.getInstance().getDataFactory().getRDFSLabel(),
                        EntityFactory.getInstance().getDataFactory().getOWLLiteral(localizedName, language));
        OWLAxiom axiom = EntityFactory.getInstance().getDataFactory().getOWLAnnotationAssertionAxiom(getOWLObject().getIRI(), commentAnnotation);
        OntologyService.getInstance().addAxiom(axiom);
        return this;
    }

    public NamedEntity<T> name(String localizedName, Locale locale) {
        TextSet name = getName();
        name.add(locale, localizedName);

        OWLAnnotation commentAnnotation = EntityFactory.getInstance().getDataFactory()
                .getOWLAnnotation(EntityFactory.getInstance().getDataFactory().getRDFSLabel(),
                        EntityFactory.getInstance().getDataFactory().getOWLLiteral(localizedName, locale.getLanguage()));
        OWLAxiom axiom = EntityFactory.getInstance().getDataFactory().getOWLAnnotationAssertionAxiom(getOWLObject().getIRI(), commentAnnotation);
        OntologyService.getInstance().addAxiom(axiom);
        return this;
    }

    public boolean isAnonymous() {
        return name != null;
    }

}
