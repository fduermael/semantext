package io.semantext.core.domain.text;

import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

public class TextSet {

    private Set<Text> set = new HashSet<Text>();


    public TextSet add(String languageIsoCode, String localizedName) {
        set.add(new Text(languageIsoCode, localizedName));
        return this;
    }

    public TextSet add(Locale locale, String localizedName) {
        set.add(new Text(locale, localizedName));
        return this;
    }

    public Set<Text> getSet() {
        return set;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TextSet that = (TextSet) o;
        return set.equals(that.set);
    }

    @Override
    public int hashCode() {
        return Objects.hash(set);
    }
}
