package io.semantext.core.domain.base;

import java.util.ArrayList;
import java.util.List;

public class TreeNode<T extends Base> {

    //TODO : use a paradigm to associate an head and a dependencyList


    private Integer level = 0;

    private T value;

    private TreeNode<T> head = null;

    private String headRelation = "ROOT";

    private List<TreeNode<T>> dependencyList = new ArrayList<>();

    public TreeNode() {
    }


    public TreeNode(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public TreeNode<T> getHead() {
        return head;
    }

    public void setHead(TreeNode<T> head) {
        this.head = head;
    }

    public List<TreeNode<T>> getDependencyList() {
        return dependencyList;
    }

    public void setDependencyList(List<TreeNode<T>> dependencyList) {
        this.dependencyList = dependencyList;
    }

    @Override
    public String toString() {
        return "TreeNode [" + getValue().getName().toString() + ", metadata=" + getValue().getMetadata().toString() +", dependencyList="+ dependencyList.toString() + "]";
    }

    public void accept(TreeNodeVisitor v) {
        v.visit(this);
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getHeadRelation() {
        return headRelation;
    }

    public void setHeadRelation(String headRelation) {
        this.headRelation = headRelation;
    }
    
}
