package io.semantext.core.domain.note;

import java.util.Date;

/**
 * typical note from Google Keep
 */
public class Note {

    private String title;

    private String text;

    private Date date;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    
    
}
