package io.semantext.core.domain.base;

import java.util.UUID;

public abstract class Base {

    /**
     * identifier
     */
    private UUID id = UUID.randomUUID();

    /**
     * lemma if relevant
     * empty otherwise
     */
    private String name = "";

   

    /**
     * metadata
     */
    private Metadata metadata = new Metadata();

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public String getType() {
        return (String)metadata.get("type");
    }

    public void setType(String type) {
        metadata.put("type",type);
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        return "Base [id=" + id + ", name=" + name + ", metadata=" + metadata.toString() + "]";
    }
 
}
