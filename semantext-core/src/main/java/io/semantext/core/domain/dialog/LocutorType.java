package io.semantext.core.domain.dialog;

public enum LocutorType {
    user,
    system
}
