package io.semantext.core.domain.base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Metadata extends HashMap<String, Object> {

    /**
     *
     */
    private static final long serialVersionUID = -6318171250039213462L;
    
    public static String separator = "/";
    public static String separatorRegex = "/";

      public Metadata() {
        super();
    }    

    public Metadata(Map<? extends String, ? extends Object> map) {
        super(map);
        //init();
      
    } 
    
    public void init() {
        // init with composite key
        
        for (String key : super.keySet()) {
            put(key, super.get(key));
        }
    }


    public Object put(String compositeKey, Object value) {
        List<String> keyList = split(compositeKey);
        if (keyList.size()==1) {
            String key = keyList.get(0);
            Object terminalValue = super.put(key, value);             
            return terminalValue;    
        } 
        else if (keyList.size()>1) {
            String firstKey = keyList.get(0);
            Object intermediateValue = super.get(firstKey);
            
            if (!(intermediateValue instanceof Metadata)) {
                intermediateValue = new Metadata();
                super.put(firstKey,intermediateValue);
            }
            List<String> newKeyList = new ArrayList<>();
            for (int i=1; i<keyList.size(); i++) {
                newKeyList.add(keyList.get(i));
            }
            return ((Metadata)intermediateValue).put(join(newKeyList), value);
        }
        else {
            return null;
        }
    }
    /**
     * 
     * @param compositeKey
     * @return
     */

    public Object get(String compositeKey) {
        List<String> keyList = split(compositeKey);
        if (keyList.size()==1) {
            Object terminalValue = super.get(keyList.get(0));
            return terminalValue;
        } 
        else if (keyList.size()>1) {
            String firstKey = keyList.get(0);
            Object intermediateValue = super.get(firstKey);
            if (intermediateValue instanceof Metadata) {
                List<String> newKeyList = new ArrayList<>();
                for (int i=1; i<keyList.size(); i++) {
                    newKeyList.add(keyList.get(i));
                }
                return ((Metadata)intermediateValue).get(join(newKeyList));
            } 
            else if (intermediateValue instanceof Map) {
                List<String> newKeyList = new ArrayList<>();
                for (int i=1; i<keyList.size(); i++) {
                    newKeyList.add(keyList.get(i));
                }
                Metadata newIntermediateValue = new Metadata((Map)intermediateValue);
                super.put(firstKey, newIntermediateValue);
                return newIntermediateValue.get(join(newKeyList));
            } 
            else {    
                return intermediateValue;
            }    
        }
        else {
            return null;
        }
    }

    /**
     * 
     * @param compositeKey
     * @return
     */

    private List<String> split(String compositeKey) {
        List<String> keyList = Arrays.asList(compositeKey.split(separatorRegex));
        return keyList;
    }

    private String join(List<String> keyList) {
        return String.join(separator, keyList);
    }

}
