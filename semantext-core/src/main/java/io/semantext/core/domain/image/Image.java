package io.semantext.core.domain.image;

import java.util.ArrayList;
import java.util.List;

import io.semantext.core.domain.base.Base;
import io.semantext.core.domain.file.File;
import io.semantext.core.domain.text.FramedText;

/**
 * image type is MIME type (image/png for instance)
 */
public class Image extends Base {

    /**
     * optional : without necessary storage
     */
    private File file;


    private List<FramedText> annotationList = new ArrayList<>();

    
    /**
     * internal rendeded content
     */
    private byte[] raw;

    public byte[] getRaw() {
        return raw;
    }

    public void setRaw(byte[] raw) {
        this.raw = raw;
    }


    public List<FramedText> getAnnotationList() {
        return annotationList;
    }

    public void setAnnotationList(List<FramedText> annotationList) {
        this.annotationList = annotationList;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
  
    
}
