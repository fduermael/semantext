package io.semantext.core.domain.speech;

public enum SpeechMetadataKey {
    beginDate,
    endDate
}
