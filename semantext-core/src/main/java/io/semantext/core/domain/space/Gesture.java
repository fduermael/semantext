package io.semantext.core.domain.space;

import io.semantext.core.domain.base.Base;

public class Gesture extends Base {

    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    
}
