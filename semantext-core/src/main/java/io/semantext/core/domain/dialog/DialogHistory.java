package io.semantext.core.domain.dialog;

import java.util.ArrayList;
import java.util.List;

/**
 * sequence of utterances (text with beginTime/endTime) by locutors
 */
public class DialogHistory {

    private List<DialogAct> list = new ArrayList<>();

    public List<DialogAct> getList() {
        return list;
    }

    public void setList(List<DialogAct> list) {
        this.list = list;
    }

    
}
