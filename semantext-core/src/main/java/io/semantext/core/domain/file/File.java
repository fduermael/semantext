package io.semantext.core.domain.file;

import java.io.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.semantext.core.domain.base.Base;

/**
 * file type is MIME type (image/png for instance)
 */
public class File extends Base {

    private byte[] raw;

    @JsonIgnore
    private InputStream inputStream;


    public File() {
        super();
    }

   
    public File(String fileName) {
        super();
        new java.io.File(fileName).getParentFile().getAbsolutePath();
        setParentFileName(new java.io.File(fileName).getParentFile().getAbsolutePath());
        setName(new java.io.File(fileName).getName());
    }

    public File(String parentFileName, String name) {
        super();
        setParentFileName(parentFileName);
        setName(name);
    }


    public String getParentFileName() {
        return (String)getMetadata().get("parentFileName");
    }

    public void setParentFileName(String parentFileName) {
        getMetadata().put("parentFileName", parentFileName);
    }

    public byte[] getRaw() {
        return raw;
    }

    public void setRaw(byte[] raw) {
        this.raw = raw;
    }

    
    public InputStream getInputStream() {
        if (inputStream != null) {
            return inputStream;    
        }
        try {
            java.io.File file = new java.io.File(new java.io.File(getParentFileName()), super.getName());
            this.inputStream = new BufferedInputStream(new FileInputStream(file));
        }
        catch (IOException e) {
            this.inputStream = null;    
        }    
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public void clean() {
        raw = null;
    }

    public void close() {
        try {
            inputStream.close();
        }
        catch (IOException e) {

        }
        inputStream = null;
    }

}
