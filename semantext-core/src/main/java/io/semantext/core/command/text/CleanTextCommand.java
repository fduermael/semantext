package io.semantext.core.command.text;

import java.util.Base64;

import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.dataaccess.system.ScriptLauncher;
import io.semantext.core.domain.text.Text;
import io.semantext.core.helper.JsonHelper;

public class CleanTextCommand extends AbstractCommand<Text, Text> {
    private static Logger logger = LoggerFactory.getLogger(CleanTextCommand.class);

    //@Inject
    ScriptLauncher scriptLauncher = new ScriptLauncher();

    @Override
    public Text executeUnit(Text text) {


        String serializedInputText = JsonHelper.serializeJson(text); 
           
        //String base64encodedInputText = BaseHelper.serializeJsonBase64(text); 
        

        //ScriptLauncher scriptLauncher = new ScriptLauncher();

        
        String serializedOutputText = scriptLauncher
                    .launchStdInOut("io/semantext/core/command/text/CleanTextCommand.py", serializedInputText);
            
        /*
        String serializedOutputText = scriptLauncher
                    .launch("io/semantext/core/command/text/CleanTextCommand.py", base64encodedInputText);
        */            
        System.out.println(serializedOutputText);
        Text deserializedOutputText = (Text)  JsonHelper.deserializeJson(serializedOutputText, Text.class); 
        return deserializedOutputText;
       
    }

}
