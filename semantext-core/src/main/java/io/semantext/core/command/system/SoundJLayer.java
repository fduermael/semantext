package io.semantext.core.command.system;

import javazoom.jl.player.advanced.AdvancedPlayer;
import javazoom.jl.player.advanced.PlaybackEvent;
import javazoom.jl.player.advanced.PlaybackListener;

import java.io.ByteArrayInputStream;

class SoundJLayer extends PlaybackListener implements Runnable
{
    private byte[] byteArray;
    private AdvancedPlayer player;
    private Thread playerThread;

    public SoundJLayer(byte[] byteArray)
    {
        this.byteArray = byteArray;
    }

    public void play()
    {
        try
        {


            this.player = new AdvancedPlayer
                    (
                            new ByteArrayInputStream(byteArray),
                            javazoom.jl.player.FactoryRegistry.systemRegistry().createAudioDevice()
                    );

            this.player.setPlayBackListener(this);

            this.playerThread = new Thread(this, "AudioPlayerThread");

            this.playerThread.start();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    // PlaybackListener members

    public void playbackStarted(PlaybackEvent playbackEvent)
    {
        System.out.println("playbackStarted at " + playbackEvent.getFrame());
    }

    public void playbackFinished(PlaybackEvent playbackEvent)
    {
        System.out.println("playbackEnded at "+ playbackEvent.getFrame());
    }

    // Runnable members

    public void run()
    {
        try
        {
            this.player.play();
        }
        catch (javazoom.jl.decoder.JavaLayerException ex)
        {
            ex.printStackTrace();
        }

    }
}

