package io.semantext.core.command.document;

import java.io.InputStream;

import org.apache.tika.Tika;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;

import io.semantext.core.command.AbstractCommand;

import io.semantext.core.domain.document.Document;
import io.semantext.core.factory.text.TikaFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//TODO 
public class ExtractDocumentMetadataCommand extends AbstractCommand<Document, Document> {

    private static Logger logger = LoggerFactory.getLogger(ExtractDocumentMetadataCommand.class);


    @Override
    public Document executeUnit(Document document) {
        Tika tika = TikaFactory.getInstance();
       
        
        try {
            InputStream inputStream = document.getFile().getInputStream();

            //TikaInputStream tis =  TikaInputStream.get(inputStream);
            //file.setType(tika.detect(tis));
            Metadata metadata = new Metadata();
            //Parser method parameters
            tika.parse(inputStream, metadata);

            String[] metadataNames = metadata.names();

            for(String name : metadataNames) {		        
                document.getFile().getMetadata().put(name,metadata.get(name));
            }
        }
        catch (Exception  e) {
            logger.error("Error while parsing stream",e);
        }
        return document;
    }
    
}
