package io.semantext.core.command.text;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.domain.document.Document;
import io.semantext.core.domain.document.Section;
import io.semantext.core.domain.document.Page;
import io.semantext.core.domain.text.FramedText;
import io.semantext.core.domain.text.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExtractParagraphInDocumentCommand extends AbstractCommand<Document,Text>{
    
    private static Logger logger = LoggerFactory.getLogger(ExtractParagraphInDocumentCommand.class);

    // detection of multiple carriage returns as a boundary of paragraph 
    String separatorRegex = "\n{2,}";


    //TODO : structured or flat

    public List<Text> executeExpand(Document document ) {
        List<Text> result = new ArrayList<>();
        if (document.getSectionList().isEmpty()&&!document.getPageList().isEmpty()) {
            structureDeeplyFromPages(document);
        }
        // make a recursive  function if subsections exist 
        // otherwise on the assumption of flat sections ==> detect boundaries (or use better tika)
        for (Section s : document.getSectionList()) {

            String rawTextString = s.getText().getRaw();

            Pattern pattern = Pattern.compile(separatorRegex);
            Matcher matcher = pattern.matcher(rawTextString);
            int index = 0;
            // Check all occurrences
            while (matcher.find()) {
                int length = matcher.start()-index;
                Text subText = new Text(s,index,length);
                result.add(subText);
                index = matcher.end();
                //System.out.println(" Found: " + matcher.group());
            }    
            Text subText = new Text(s,index,rawTextString.length()-index);

            result.add(subText);
        }
        return result;
    }

    //TODO : glue paragraphs on two consecutives pages
    // and perform a bottom up strategy
    private void structureDeeplyFromPages(Document document) {
        for (Page page : document.getPageList()) {
            for (FramedText framedText : page.getFramedTextList()) {
                Section s = new Section(document,framedText.getText());
                s.setType("page"); // temporary, shoud not exist
            }
        }
    }


    

}
