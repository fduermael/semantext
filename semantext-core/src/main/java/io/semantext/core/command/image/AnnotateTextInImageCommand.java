package io.semantext.core.command.image;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.dataaccess.system.ScriptLauncher;
import io.semantext.core.domain.file.File;
import io.semantext.core.domain.image.*;
import io.semantext.core.domain.text.*;
import io.semantext.core.domain.text.FramedText;

public class AnnotateTextInImageCommand extends AbstractCommand<File, Image> {
    private static Logger logger = LoggerFactory.getLogger(AnnotateTextInImageCommand.class);

    // @Inject
    ScriptLauncher scriptLauncher = new ScriptLauncher();

    @Override
    public Image executeUnit(File file) {
        // TODO : use Eclipse MicroProfile

        // ScriptLauncher scriptLauncher = new ScriptLauncher();

        String recognizedString = scriptLauncher.launch(
                "ExtractPlainTextInImageCli.py",
                file.getParentFileName() + "/" + file.getName());
        Image image = new Image();
        image.setFile(file);
        // stringList.forEach(s -> {
        FramedText annotation = new FramedText();
        Text text = new Text(recognizedString);
        annotation.setText(text);
        image.getAnnotationList().add(annotation);
        // });
        return image;
    }

}