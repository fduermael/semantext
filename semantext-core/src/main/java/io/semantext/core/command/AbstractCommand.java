package io.semantext.core.command;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.NotImplementedException;

public abstract class AbstractCommand<I, O> {

    public enum Multiplicity {
        zero,
        one,
        many
    }

    /**
     * input parametrized type
     */
    private I input;

    /**
     * output parametrized type
     */
    private O output;

    /**
     * input list
     */
    private List<I> inputList;

    /**
     * output list
     */
    private List<O> outputList;


    /** 
     * parameters of the command 
     * */
    private Map<String,Object> parameterMap = new HashMap<>();

    /** 
     * parameters classes of the command 
     * */
    private Map<String,Class<?>> parameterClassMap = new HashMap<>();

    private Multiplicity inputMultiplicity = Multiplicity.one;

    private Multiplicity outputMultiplicity = Multiplicity.one;

    public I getInput() {
        return input;
    }

    public void setInput(I input) {
        this.input = input;
    }

    public O getOutput() {
        return output;
    }

    public void setOutput(O output) {
        this.output = output;
    }

    public List<I> getInputList() {
        return inputList;
    }

    public void setInputList(List<I> inputList) {
        this.inputList = inputList;
    }

    public List<O> getOutputList() {
        return outputList;
    }

    public void setOutputList(List<O> outputList) {
        this.outputList = outputList;
    }
    
    public Multiplicity getInputMultiplicity() {
        return inputMultiplicity;
    }

    public void setInputMultiplicity(Multiplicity inputMultiplicity) {
        this.inputMultiplicity = inputMultiplicity;
    }

    public Multiplicity getOutputMultiplicity() {
        return outputMultiplicity;
    }

    public void setOutputMultiplicity(Multiplicity outputMultiplicity) {
        this.outputMultiplicity = outputMultiplicity;
    }

    
    public void execute() {
        if (Multiplicity.one.equals(inputMultiplicity)) {
            if (Multiplicity.one.equals(outputMultiplicity)) {
                setOutput(executeUnit(getInput()));
            }
            else if (Multiplicity.many.equals(outputMultiplicity)) {
                setOutputList(executeExpand(getInput()));
            }    
        }
        else {
            if (Multiplicity.one.equals(outputMultiplicity)) {
                setOutput(executeReduce(getInputList()));
            }
            else if (Multiplicity.many.equals(outputMultiplicity)) {
                setOutputList(getInputList()
                .parallelStream()
                .map( i -> executeUnit(i) )
                .collect(Collectors.toList()));
            }               
        }

    }

    public O executeUnit(I i) {
        throw new NotImplementedException("execute unit");
    }

    public List<O> executeExpand(I i) {
        throw new NotImplementedException("execute expand");
    }


    public O executeReduce(List<I> i) {
        throw new NotImplementedException("execute reduce");
    }


    public Map<String, Object> getParameterMap() {
        return parameterMap;
    }

    public void setParameterMap(Map<String, Object> parameterMap) {
        this.parameterMap = parameterMap;
    }

    public Map<String, Class<?>> getParameterClassMap() {
        return parameterClassMap;
    }

    public void setParameterClassMap(Map<String, Class<?>> parameterClassMap) {
        this.parameterClassMap = parameterClassMap;
    }

    /**
     * DSL
     */
    public AbstractCommand inputMultiplicity(Multiplicity m) {
        setInputMultiplicity(m);
        return this;    
    }

    public AbstractCommand outputMultiplicity(Multiplicity m) {
        setOutputMultiplicity(m);
        return this;    
    }

    public AbstractCommand parameter(String name, Object value) {
        getParameterMap().put(name, value);
        return this;    
    }

    public AbstractCommand parameterClass(String name, Class<?> clazz) {
        getParameterClassMap().put(name, clazz);
        return this;    
    }


}
