package io.semantext.core.command.image;

import java.util.List;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.domain.file.File;
import io.semantext.core.domain.image.Image;
import io.semantext.core.helper.ImageHelper;
import io.semantext.core.helper.PdfBoxHelper;

import java.awt.image.RenderedImage;
import java.io.*;

import java.util.ArrayList;

import java.util.stream.Collectors;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;


public class ExtractImageListInPdfFileCommand extends AbstractCommand<File, List<Image>> {

    private Integer pageNumber = 0;

    @Override
    public List<Image> executeUnit(File file) {
        List<Image> images = new ArrayList<>();
        PDDocument document;
        try {
            document = PDDocument.load(file.getInputStream());

            PDPage page = document.getPages().get(pageNumber);
            List<RenderedImage> renderedImageList = PdfBoxHelper.getImagesFromResources(page.getResources());
            document.close();
            List<byte[]> imageByteArrayList = renderedImageList.stream().map(ImageHelper::toByteArray)
                    .collect(Collectors.toList());

            for (int j = 1; j <= imageByteArrayList.size(); j++) {
                Image image = new Image();
                image.setType("png");
                image.setRaw(imageByteArrayList.get(j - 1));
                // image.getAnnotationList().addAll(googleCloudVisionTextDetectionService.detectText(imageByteArrayList.get(j
                // - 1)));
                images.add(image);
            }
        } catch (IOException e) {
           throw new RuntimeException(e);
        }
        return images;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

}
