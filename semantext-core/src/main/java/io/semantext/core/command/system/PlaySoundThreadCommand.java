package io.semantext.core.command.system;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import io.semantext.core.command.AbstractCommand;

public class PlaySoundThreadCommand extends AbstractCommand<byte[],Void>{

     private static Logger LOG = LoggerFactory
            .getLogger(PlaySoundThreadCommand.class);

    
    @Override
    public Void executeUnit(byte[] mp3ByteArray) {

            try {
                SoundJLayer soundToPlay = new SoundJLayer(mp3ByteArray);

                soundToPlay.play();
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }

            return null;
    }

}
