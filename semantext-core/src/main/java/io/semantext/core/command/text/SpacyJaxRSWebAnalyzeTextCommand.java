package io.semantext.core.command.text;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

//import org.slf4j.Logger;

//import org.slf4j.LoggerFactory;

import io.semantext.core.command.AbstractCommand;

import io.semantext.core.domain.text.Text;
import io.semantext.core.domain.text.TextList;
import io.semantext.core.helper.JsonHelper;


public class SpacyJaxRSWebAnalyzeTextCommand extends AbstractCommand<Text, Text> {
    //private static Logger logger = LoggerFactory.getLogger(CleanTextCommand.class);

    private Client client = ClientBuilder.newClient();


    @Override
    public Text executeUnit(Text text) {     
        
        // javax.ws.rs.client.ResponseProcessingException: javax.ws.rs.ProcessingException: RESTEASY003145: Unable to find a MessageBodyReader of content-type text/string and type class io.semantext.core.domain.text.Text


        TextList textList = client
            .target("http://localhost:8000")
            .path("/sents")
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.json(text),TextList.class);

        List entList = client
             .target("http://localhost:8000")
             .path("/entities")
             .request(MediaType.APPLICATION_JSON)
             .post(Entity.json(text),List.class);

       
      
        //todo : return a list<Text>
        //TextList deserializedOutputText = (TextList)  BaseHelper.deserializeJson(serializedOutputText, TextList.class); 

       
        text.setComponentList(textList);

        text.getMetadata().put("sem_ent",entList);

        return text;
       
    }

}
