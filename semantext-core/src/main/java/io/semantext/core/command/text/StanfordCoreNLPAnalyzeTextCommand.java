package io.semantext.core.command.text;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.domain.text.Text;
import io.semantext.core.domain.text.TextMetadataKey;
import io.semantext.core.domain.text.TextType;
import io.semantext.core.factory.text.StanfordCoreNLPAnalyzerFactory;

import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;


import edu.stanford.nlp.ling.CoreAnnotations;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.naturalli.NaturalLogicAnnotations;
import edu.stanford.nlp.pipeline.Annotation;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.util.CoreMap;


public class StanfordCoreNLPAnalyzeTextCommand extends AbstractCommand<Text, Text> {


    private static Logger logger = LoggerFactory
            .getLogger(StanfordCoreNLPAnalyzeTextCommand.class);

    
    /**
     * TODO : option parametrize with type of analyzer
     */

    @Override
    public Text executeUnit(Text paragraphAsText) {
        //paragraphAsText.setType(TextType.paragraph.name());
        
        Annotation annotation = new Annotation(paragraphAsText.getRaw());

        //TODO detect language automatically
        if (paragraphAsText.getLang() == null || paragraphAsText.getLang().isEmpty()) {
            paragraphAsText.setLang(Locale.getDefault().getLanguage());
        }

        StanfordCoreNLP pipeline = StanfordCoreNLPAnalyzerFactory.getInstance(paragraphAsText.getLang());
        pipeline.annotate(annotation);

        annotation.get(CoreAnnotations.SentencesAnnotation.class).forEach(sentence -> {
            Text sentenceAsText = new Text(paragraphAsText.getContainer(), sentence.get(TextAnnotation.class).toString());  
            sentenceAsText.setType(TextType.sentence.name());
            paragraphAsText.getComponentList().add(sentenceAsText);
            sentenceAsText.setLang(paragraphAsText.getLang());

            sentence.get(CoreAnnotations.TokensAnnotation.class).forEach(token -> {
                logger.info(token.toShortString());
                Text tokenAsText = new Text(token.originalText());  //word() or value()
                tokenAsText.setType(TextType.token.name());
                sentenceAsText.getComponentList().add(tokenAsText);

                tokenAsText.setName(token.lemma());
                tokenAsText.setLang(sentenceAsText.getLang());
                //tokenAsText.setRaw(token.originalText());
                tokenAsText.getMetadata().put(TextMetadataKey.pos.name(), token.tag());
                tokenAsText.getMetadata().put(TextMetadataKey.index.name(), token.index());
                //tokenAsText.getMetadata().put("category", token.category());
            
                //tokenAsText.getMetadata().put("feat",token.get(CoreAnnotations.FeaturesAnnotation.class).toString());
                tokenAsText.getMetadata().put("sem/ent/type", token.ner());
                tokenAsText.getMetadata().put("sem/ent/confidence", token.nerConfidence());

            });

            
            enrichWithRelationTriples(sentence,sentenceAsText);

            SemanticGraph graph = sentence.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);
            logger.info(graph.toString(SemanticGraph.OutputFormat.LIST));

            List<SemanticGraphEdge> edgeList = graph.edgeListSorted();
            graph.getRoots().forEach( 
                // should have only one root
                root -> {                   
                    if (root.index()>=1) {
                        Text rootText = sentenceAsText.getComponentList().get(root.index()-1);
                        rootText.getMetadata().put("dep/head/relation","ROOT");
                        rootText.getMetadata().put("dep/head/confidence",1);
                        rootText.getMetadata().put("dep/head/index",-1);
                    }
                    buildDependency(sentenceAsText.getComponentList(), edgeList, root);
                });    
             
   
        });
        return paragraphAsText;
    }    
    



    private static void buildDependency(List<Text> globalList, List<SemanticGraphEdge> edgeList, IndexedWord head) {
        
        edgeList.forEach(
            edge -> {
                if (edge.getSource().equals(head)) {
                    Text sourceText = null;
                    if (edge.getSource().index()>=1) {
                        sourceText = globalList.get(edge.getSource().index()-1);
                    }
                    Text targetText = null;
                    if (edge.getTarget().index()>=1) {
                        targetText = globalList.get(edge.getTarget().index()-1);
                        targetText.getMetadata().put("dep/head/relation",edge.getRelation().toString());
                        targetText.getMetadata().put("dep/head/score",edge.getWeight());
                        targetText.getMetadata().put("dep/head/index",sourceText.getMetadata().get(TextMetadataKey.index.name()));
                        logger.debug(sourceText.getRaw() +" set as head to "+targetText.getRaw() +" with relation "+ edge.getRelation().toString());                        
                    }    
                    buildDependency(globalList,  edgeList, edge.getTarget());
                } 
            }
        );
        
    }

    private static void enrichWithRelationTriples(CoreMap sentence, Text sentenceText) {
        Collection<edu.stanford.nlp.ie.util.RelationTriple> triples = sentence.get(NaturalLogicAnnotations.RelationTriplesAnnotation.class);
        if (triples == null) {
            return;
        }
       
        for (edu.stanford.nlp.ie.util.RelationTriple triple : triples) {

            logger.info(triple.confidence + "\t" +
            triple.subjectLemmaGloss() + "\t" +
            triple.relationLemmaGloss() + "\t" +
            triple.objectLemmaGloss());

            sentenceText.getMetadata().put("sem/triple/subject", triple.subjectLemmaGloss());
            sentenceText.getMetadata().put("sem/triple/relation", triple.relationLemmaGloss());
            sentenceText.getMetadata().put("sem/triple/object", triple.objectLemmaGloss());
            sentenceText.getMetadata().put("sem/triple/confidence", triple.confidence);

        }

        

    }

}
