package io.semantext.core.command.file;

import org.apache.lucene.search.*;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Fields;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import io.semantext.core.command.AbstractCommand;
import io.semantext.core.domain.file.File;
import io.semantext.core.helper.ConfigHelper;

/**
 * image is supposed to habe bean built with metadata
 */
public class SearchIndexDocumentCommand extends AbstractCommand<String, File> {

    private static Logger logger = LoggerFactory.getLogger(SearchIndexDocumentCommand.class);

    private static StandardAnalyzer analyzer = new StandardAnalyzer();

    @Override
    public List<File> executeExpand(String queryString) {
        List<File> result = new ArrayList<>();
        try {
            logger.info("begin search with :"+queryString);
            FSDirectory dir = FSDirectory.open(Path.of(ConfigHelper.getStringValue("index.dir")));
            IndexReader reader = DirectoryReader.open(dir);

            IndexSearcher searcher = new IndexSearcher(reader);
            //TopScoreDocCollector collector = TopScoreDocCollector.create(10, 100);

            // https://stackoverflow.com/questions/2005084/how-to-specify-two-fields-in-lucene-queryparser
            QueryParser queryParser = new QueryParser("name", analyzer);
            Query q = queryParser.parse(queryString);

            TopDocs topDocs = searcher.search(q,100);
            //List<ScoreDoc> hitList = Arrays.asList(collector.topDocs().scoreDocs);
            logger.info("hits:"+topDocs.totalHits.toString());
            for (ScoreDoc hit : topDocs.scoreDocs) {
                File f = new File(searcher.doc(hit.doc).get("parentFileName"), searcher.doc(hit.doc).get("name"));
                logger.info(f.getParentFileName()+'/'+f.getName()+" found.");
                result.add(f);

            }
            reader.close();

        } catch (IOException | ParseException e) {
            logger.error("search",e);
            e.printStackTrace();
        }        
        return result;
                 
    }    
    
}
