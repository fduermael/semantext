package io.semantext.core.command.text;

import javax.inject.Inject;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.dataaccess.restclient.SpacyAnalyzeTextService;
import io.semantext.core.domain.text.Text;
import io.semantext.core.domain.text.TextList;

public class SpacyRestAnalyzeTextCommand extends AbstractCommand<Text, Text> {
    // private static Logger logger =
    // LoggerFactory.getLogger(CleanTextCommand.class);

    @Inject
    @RestClient
    SpacyAnalyzeTextService spacyAnalyzeTextService;

    @Override
    public Text executeUnit(Text text) {
        //text is a paragraph with raw information
        TextList textList = spacyAnalyzeTextService.sents(text);
        //text = spacyAnalyzeTextService.entities(text);
        text.setComponentList(textList);
        return text;
       
    }

}
