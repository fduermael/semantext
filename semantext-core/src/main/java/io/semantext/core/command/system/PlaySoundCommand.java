package io.semantext.core.command.system;



import java.io.ByteArrayInputStream;
import javazoom.jl.player.AudioDevice;
import javazoom.jl.player.JavaSoundAudioDeviceFactory;
import javazoom.jl.player.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.semantext.core.command.AbstractCommand;


public class PlaySoundCommand extends AbstractCommand<byte[],Void>{

//TODO
// create a Sound domain object


    private static Logger LOG = LoggerFactory
            .getLogger(PlaySoundCommand.class);




    @Override
    public Void executeUnit(byte[] mp3ByteArray) {

            try {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(mp3ByteArray);
                AudioDevice javaSoundAudioDevice = new JavaSoundAudioDeviceFactory().createAudioDevice();
                Player  player = new Player(byteArrayInputStream,javaSoundAudioDevice);
                player.play();
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }

            return null;
    }

}
