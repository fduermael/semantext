package io.semantext.core.command.document;

import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.PDFTextStripper;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.domain.document.Document;
import io.semantext.core.domain.document.Page;
import io.semantext.core.domain.image.Image;
import io.semantext.core.domain.text.Text;
import io.semantext.core.domain.text.FramedText;
import io.semantext.core.domain.file.File;
import io.semantext.core.helper.ImageHelper;
import io.semantext.core.helper.PdfBoxHelper;


public class ExtractTextAndImageInPdfFileCommand extends AbstractCommand<File, Document> {

    @Override
    public Document executeUnit(File file) {

        Document document = new Document();
        PDDocument pdDocument = null;
        try {
            pdDocument = PDDocument.load(file.getInputStream());

            Integer numberOfPages = pdDocument.getNumberOfPages();
            for (int i = 1; i <= numberOfPages; i++) {
               
               
                PDFTextStripper stripper = new PDFTextStripper();
                stripper.setStartPage(i);
                stripper.setEndPage(i);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                Writer writer = new PrintWriter(byteArrayOutputStream);
                stripper.writeText(pdDocument, writer);
                writer.close();
            
                Page page = new Page(document);

                FramedText framedText = new FramedText();
                framedText.setText(new Text(new String(byteArrayOutputStream.toByteArray())));
                page.getMetadata().put("number", i);                
                page.getFramedTextList().add(framedText);
                
                PDPage pdPage = pdDocument.getPages().get(i - 1);
                List<RenderedImage> renderedImageList = PdfBoxHelper.getImagesFromResources(pdPage.getResources());
                List<byte[]> imageByteArrayList = renderedImageList.stream().map(ImageHelper::toByteArray)
                        .collect(Collectors.toList());

                for (int j = 1; j <= imageByteArrayList.size(); j++) {
                    Image image = new Image();
                    // image.setFormat(options.getImageFormat());
                    // if (options.isImageByteArray()) {
    
                    image.setRaw(imageByteArrayList.get(j - 1));
                    // }
                    // if (options.isAnnotations()) {
                    // image.getAnnotations().addAll(googleCloudVisionTextDetectionService.detectText(imageByteArrayList.get(j
                    // - 1)));
                    // }
                    page.getImageList().add(image);
                }
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return document;

    }
}