package io.semantext.core.command.text;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.client.jaxrs.internal.ResteasyClientBuilderImpl;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.plugins.providers.jackson.ResteasyJackson2Provider;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.jboss.resteasy.spi.ResteasyProviderFactory;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.dataaccess.restclient.SpacyAnalyzeTextService;
import io.semantext.core.domain.text.Text;
import io.semantext.core.domain.text.TextList;


public class SpacyResteasyAnalyzeTextCommand extends AbstractCommand<Text, Text> {
    //private static Logger logger = LoggerFactory.getLogger(CleanTextCommand.class);



    @Override
    public Text executeUnit(Text text) {     
        
        // javax.ws.rs.client.ResponseProcessingException: javax.ws.rs.ProcessingException: RESTEASY003145: Unable to find a MessageBodyReader of content-type text/string and type class io.semantext.core.domain.text.Text
        ResteasyProviderFactory instance=ResteasyProviderFactory.getInstance();
        RegisterBuiltin.register(instance);
        instance.registerProvider(ResteasyJackson2Provider.class);

        ResteasyClient restEasyClient = new ResteasyClientBuilderImpl().build();
        ResteasyWebTarget target = restEasyClient.target(UriBuilder.fromPath("http://localhost:8000"));
        SpacyAnalyzeTextService proxy = target.proxy(SpacyAnalyzeTextService.class);

        TextList textList = proxy.sents(text);

        List entList = proxy.entities(text);

        text.getMetadata().put("sem_ent", entList);
        text.setComponentList(textList);
        return text;
       
    }

}
