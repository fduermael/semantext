package io.semantext.core.command.document;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.domain.document.Document;
import io.semantext.core.helper.ConfigHelper;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.*;
import org.apache.lucene.util.*;

import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * to use with elastic search : https://stackoverflow.com/questions/38361533/an-spi-class-of-type-org-apache-lucene-codecs-codec-with-name-lucene54-does-no
 */
public class ReadIndexDocumentCommand extends AbstractCommand<Void, Document> {

    private static Logger logger = LoggerFactory.getLogger(ReadIndexDocumentCommand.class);

    private static StandardAnalyzer analyzer = new StandardAnalyzer();

    @Override
    public List<Document> executeExpand(Void v) {
        List<Document> result = new ArrayList<>();
        try {

            FSDirectory dir = FSDirectory.open(Path.of(ConfigHelper.getStringValue("index.dir")));
            IndexReader reader = DirectoryReader.open(dir);
            Integer from = (Integer)getParameterMap().get("from");
            if (from == null) {
                from = 0;
            }
            else if (from >= reader.maxDoc()) {
                from =reader.maxDoc()-1;
            }
            Integer to = (Integer)getParameterMap().get("to");
            if (to == null) {
                to = reader.maxDoc()-1;
            }
            else if (to >= reader.maxDoc()) {
                to =reader.maxDoc()-1;
            }
            for (int i=from; i<to+1; i++) {
                Document document = new Document();
                //if (reader.isDeleted(i))
                //    continue;
                logger.info("Doc:" + i);
                //Document doc = reader.document(i);
                //String docId = doc.get("docId");

                // do something with docId here...
                Fields fields = reader.getTermVectors(i);
                if (fields != null) {
                    for (String field : fields) {
                        Terms terms = fields.terms(field);
                        TermsEnum termsEnum = terms.iterator();

                        while(true) {
                            final BytesRef term = termsEnum.next();
                            if (term == null ) {
                                break;
                            }
                            String text = term.utf8ToString();
                            document.getMetadata().put(field, text);
                            logger.info("Term:" + text);
                        }

                    }
                }
                result.add(document);
            }


            reader.close();

        } catch (IOException e) {
            logger.error("read",e);
            e.printStackTrace();
        }        
        return result;
                 
    }    
    
}
