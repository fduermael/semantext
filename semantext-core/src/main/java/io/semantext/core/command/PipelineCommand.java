package io.semantext.core.command;

import java.util.ArrayList;
import java.util.List;

public class PipelineCommand extends AbstractCommand<Object,Object> {

    private List<AbstractCommand> commandList = new ArrayList<AbstractCommand>();

    public Object executeUnit (Object input) {
        Object currentInput = input;
        AbstractCommand currentCommand = commandList.get(0);
        if (AbstractCommand.Multiplicity.one.equals(currentCommand.getInputMultiplicity())) {
            currentCommand.setInput(currentInput);
        }
        else {
            currentCommand.setInputList((List)currentInput);
        }
        if (commandList.size()>1) {
            for (int i=1; i<commandList.size(); i++) {
     
                currentCommand.execute();
                AbstractCommand nextCommand = commandList.get(i);           
            
                if (AbstractCommand.Multiplicity.one.equals(currentCommand.getOutputMultiplicity())) {
                    if (AbstractCommand.Multiplicity.one.equals(nextCommand.getInputMultiplicity())) {

                        nextCommand.setInput(currentCommand.getOutput());
                    }
        
                    else {
                        nextCommand.setInputList((List)currentCommand.getOutput());
                    }
                }
                else {
                    if (AbstractCommand.Multiplicity.one.equals(nextCommand.getInputMultiplicity())) {
                        nextCommand.setInput(currentCommand.getOutputList());
                    }
        
                    else {
                        nextCommand.setInputList((List)currentCommand.getOutputList());
                    }
                }
                currentCommand = nextCommand;
              
            }
        }
        
        currentCommand.execute();
        if (AbstractCommand.Multiplicity.one.equals(currentCommand.getOutputMultiplicity())) {             
            currentInput = currentCommand.getOutput();             
        }
        else {
            currentInput = currentCommand.getOutputList();
        }
    
        return currentInput;
    }

    /**
     * DSL to chain commands
     * @param command
     * @return
     */
    public PipelineCommand add(AbstractCommand command) {
        commandList.add(command);
        return this;
    }

    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

    public void setCommandList(List<AbstractCommand> commandList) {
        this.commandList = commandList;
    }

    

}
