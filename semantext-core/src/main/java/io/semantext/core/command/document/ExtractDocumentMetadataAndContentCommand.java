package io.semantext.core.command.document;

import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedInputStream;

import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.language.LanguageIdentifier;
import org.apache.tika.metadata.Metadata;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.domain.document.Document;
import io.semantext.core.domain.document.Section;
import io.semantext.core.domain.file.File;
import io.semantext.core.factory.text.TikaFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//TODO 
public class ExtractDocumentMetadataAndContentCommand extends AbstractCommand<File, Document> {

    private static Logger logger = LoggerFactory.getLogger(ExtractDocumentMetadataAndContentCommand.class);


    @Override
    public Document executeUnit(File file) {
        Document document = new Document();
        Tika tika = TikaFactory.getInstance();
        Metadata metadata = new Metadata();
        
        try {
            InputStream inputStream = file.getInputStream();

            TikaInputStream tis =  TikaInputStream.get(inputStream);
            document.setType(tika.detect(tis));
            document.setFile(file);

            String content = tika.parseToString(inputStream, metadata);
            Section section = new Section(document,content);

            //TODO: use LanguageDetector
            LanguageIdentifier languageIdentifier = new LanguageIdentifier(content);
            section.getText().setLang(languageIdentifier.getLanguage());
            String[] metadataNames = metadata.names();

            for(String name : metadataNames) {		        
                document.getMetadata().put(name,metadata.get(name));
            }
        }
        catch (IOException|TikaException e) {
            logger.error("Error while parsing stream",e);
        }
        return document;
    }
    
}
