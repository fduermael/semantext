package io.semantext.core.command.note;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.dataaccess.system.ScriptLauncher;
import io.semantext.core.domain.note.NoteCollectionMap;

public class ExtractNoteInGoogleKeepCommand extends AbstractCommand<List<String>, NoteCollectionMap> {
    private static Logger logger = Logger.getLogger(ExtractNoteInGoogleKeepCommand.class.getName());

    // @Inject
    ScriptLauncher scriptLauncher = new ScriptLauncher();

    ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public NoteCollectionMap executeUnit(List<String> labels) {

        String serializedOutputText = scriptLauncher.launch("ExtractNoteInGoogleKeepCli.py",
                (String) getParameterMap().get("user"), (String) getParameterMap().get("password"),
                String.join(",", labels));

        try {
            return objectMapper.readValue(serializedOutputText, NoteCollectionMap.class);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            throw new RuntimeException(e);
        }

    }

}
