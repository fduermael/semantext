package io.semantext.core.command.system;

import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.time.Instant;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import io.semantext.core.command.AbstractCommand;
import io.semantext.core.domain.document.Document;
import io.semantext.core.helper.ConfigHelper;
import io.semantext.core.helper.StringHelper;

/**
 * image is supposed to habe bean built with metadata
 */
public class AddIndexDocumentCommand extends AbstractCommand<Document, Void> {

    private static Logger logger = LoggerFactory.getLogger(AddIndexDocumentCommand.class);

    private static StandardAnalyzer analyzer = new StandardAnalyzer();
    
    @Override
    public Void executeUnit(Document image) {
        // https://examples.javacodegeeks.com/core-java/apache/lucene/lucene-indexing-example/
        try {
            IndexWriterConfig config = new IndexWriterConfig(analyzer);
            FSDirectory dir = FSDirectory.open(Path.of(ConfigHelper.getStringValue("index.dir")));

            IndexWriter writer = new IndexWriter(dir, config);

            org.apache.lucene.document.Document luceneDocument = new org.apache.lucene.document.Document();
            TextField nameField = new TextField("name",StringHelper.camelToSnake((String)image.getName()), Field.Store.YES);
            luceneDocument.add(nameField);
            TextField parentFileNameField = new TextField("parentFileName",StringHelper.camelToSnake((String)image.getMetadata().get("parentFileName")), Field.Store.YES);
            luceneDocument.add(parentFileNameField);
            String date = (String)image.getMetadata().get("date");
            if (date != null) {
                if (!date.endsWith("Z")) {
                    date = date + "Z";
                }
                StringField dateField = new StringField("date", DateTools.timeToString(Instant.parse(date).getEpochSecond()*1000,
                DateTools.Resolution.MONTH), Field.Store.YES);
                luceneDocument.add(dateField);
            }
            String geoLat = (String)image.getMetadata().get("geo:lat");
            if (geoLat != null) {
                StringField geoLatField = new StringField("geo.lat", geoLat, Field.Store.YES);
                luceneDocument.add(geoLatField);
            }
            String geoLong = (String)image.getMetadata().get("geo:long");
            if (geoLong != null) {
                StringField geoLongField = new StringField("geo.long", geoLong, Field.Store.YES);
                luceneDocument.add(geoLongField);
            }
            logger.info("Index document with "+luceneDocument.getFields().size()+ " fields");
            //for (IndexableField f : luceneDocument.getFields()) {
            //    logger.info(f.name()+":"+f.getCharSequenceValue().toString());
            //}
            writer.addDocument(luceneDocument);
            //writer.flush();
            //writer.commit();
            writer.close();
        }
        catch (IOException e) {
            logger.error("index",e);
        }

        return null;
        
    }    
    
}
