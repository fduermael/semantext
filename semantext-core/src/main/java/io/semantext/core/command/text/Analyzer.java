package io.semantext.core.command.text;

public enum Analyzer {
    spacy,
    spacy_restclient,
    spacy_resteasyclient,
    spacy_jaxrswebclient,
    stanford_corenlp
    
}
