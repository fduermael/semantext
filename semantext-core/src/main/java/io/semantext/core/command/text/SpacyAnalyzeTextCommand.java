package io.semantext.core.command.text;

import java.util.logging.Logger;

import javax.inject.Inject;


import io.semantext.core.command.AbstractCommand;

import io.semantext.core.dataaccess.system.ScriptLauncher;
import io.semantext.core.domain.text.Text;
import io.semantext.core.helper.JsonHelper;

public class SpacyAnalyzeTextCommand extends AbstractCommand<Text, Text> {
    private static Logger logger = Logger.getLogger(SpacyAnalyzeTextCommand.class.getName());

    //@Inject
    ScriptLauncher scriptLauncher = new ScriptLauncher();


    @Override
    public Text executeUnit(Text text) {
       
        String serializedInputText = JsonHelper.serializeJson(text); 
           
        //String base64encodedInputText = BaseHelper.serializeJsonBase64(text); 
        

        //ScriptLauncher scriptLauncher = new ScriptLauncher();

        
        String serializedOutputText = scriptLauncher
                    .launchStdInOut("SpacyAnalyzeTextCli.py", serializedInputText);
            
        /*
        String serializedOutputText = scriptLauncher
                    .launch("io/semantext/core/command/text/CleanTextCommand.py", base64encodedInputText);
        */            
        System.out.println(serializedOutputText);
        Text deserializedOutputText = (Text)  JsonHelper.deserializeJson(serializedOutputText, Text.class); 

        /*
        deserializedOutputText.getComponentList().forEach(sentence ->
        {
            // compute 
            sentence.getComponentList().forEach( token -> {
                System.out.println(token.getMetadata().get("dep_head_relation"));
                //token.setMetadata(new Metadata(token.getMetadata()));
            });
        }
        );
        */

        return deserializedOutputText;
       
    }

}
