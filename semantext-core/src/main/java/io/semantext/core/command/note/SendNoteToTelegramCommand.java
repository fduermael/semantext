package io.semantext.core.command.note;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.inject.Inject;


import io.semantext.core.command.AbstractCommand;
import io.semantext.core.dataaccess.system.ScriptLauncher;
import io.semantext.core.domain.text.Text;
import io.semantext.core.helper.JsonHelper;

public class SendNoteToTelegramCommand extends AbstractCommand<Text, Void> {
    private static Logger logger = Logger.getLogger(SendNoteToTelegramCommand.class.getName());

    //@Inject
    ScriptLauncher scriptLauncher = new ScriptLauncher();


    @Override
    public Void executeUnit(Text text) {
       
        Map parameters =   new HashMap(getParameterMap());
        parameters.put("user_messages",Arrays.asList(text.getRaw()));

        String serializedInputText = JsonHelper.serializeJson(parameters);
           
        
        
        scriptLauncher
                    .launchStdInOut("SendNoteToTelegramCli.py", serializedInputText);
            
       return null;
       
    }

}
