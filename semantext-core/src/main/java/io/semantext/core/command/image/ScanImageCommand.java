package io.semantext.core.command.image;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.domain.image.Image;

public class ScanImageCommand extends AbstractCommand<Void, Image>{


    private static Logger logger = LoggerFactory
            .getLogger(ScanImageCommand.class);

    
    

    @Override
    public Image executeUnit(Void t) {
        Runtime runtime1 = Runtime.getRuntime();
        try {
            Process process1 = runtime1.exec("scanimage --format=jpg");

            byte[] byteArray =  process1.getInputStream().readAllBytes();


            try (FileOutputStream fileOutputStream = new FileOutputStream(new java.io.File("/tmp/test.jpg"))) {
                fileOutputStream.write(byteArray);
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }

            Image image = new Image();

            image.setRaw(byteArray);
            image.setType("image/jpg");
            return image;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
