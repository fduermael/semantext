package io.semantext.core.command.text;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Writer;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.tika.language.LanguageIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.semantext.core.command.AbstractCommand;
import io.semantext.core.domain.text.Text;
import io.semantext.core.domain.file.File;


public class ExtractTextInPdfFileCommand extends AbstractCommand<File, Text> {

    Logger logger = LoggerFactory.getLogger("ExtractTextInPdfFileCommand");

    private Integer startPage = 0;

    private Integer endPage = -1;

    @Override
    public Text executeUnit(File file) {
        Text text = null;
        PDDocument document;
        try {
            
            document = PDDocument.load(file.getInputStream());

            PDFTextStripper stripper = new PDFTextStripper();
            stripper.setStartPage(startPage);
            if (endPage==-1) {
                endPage = document.getNumberOfPages();
            }
            stripper.setEndPage(endPage);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            Writer writer = new PrintWriter(byteArrayOutputStream);
            stripper.writeText(document, writer);
            writer.close();
            document.close();

            String content = new String(byteArrayOutputStream.toByteArray()); 
            LanguageIdentifier languageIdentifier = new LanguageIdentifier(content);
            String lang = languageIdentifier.getLanguage();
            logger.info("lang:"+lang);
            text = new Text(lang,content);
    
        } catch (IOException e) {
           throw new RuntimeException(e);
        }
        return text;
    }

    public Integer getStartPage() {
        return startPage;
    }

    public void setStartPage(Integer startPage) {
        this.startPage = startPage;
    }

    public Integer getEndPage() {
        return endPage;
    }

    public void setEndPage(Integer endPage) {
        this.endPage = endPage;
    }

   
}
