package io.semantext.core.dataaccess.system;

import java.io.ByteArrayOutputStream;

import java.io.InputStream;

import java.nio.charset.Charset;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.input.CharSequenceInputStream;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;

import io.semantext.core.helper.ConfigHelper;

//@ApplicationScoped
// exchange data between stdin and stdout
// https://docs.python.org/3/library/venv.html
public class ScriptLauncher {

    Logger logger = Logger.getLogger(ScriptLauncher.class.getName());

    
    //@Inject
    //Config config;



    public String launch(String scriptName, String... arguments) {
        String line = ConfigHelper.getStringValue("script.engine") + " " + ConfigHelper.getStringValue("script.source") + scriptName;          
        int exitCode = 0;
        CommandLine shellCommand =  null;       

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            shellCommand = CommandLine.parse(line);
            shellCommand.addArguments(arguments);

            PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);

            DefaultExecutor executor = new DefaultExecutor();
            executor.setStreamHandler(streamHandler);

            //TODO : test if windows
            //executor.execute(CommandLine.parse("Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser"));    
            DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
            executor.execute(shellCommand, resultHandler);
            resultHandler.waitFor();
            exitCode = resultHandler.getExitValue();

            if (exitCode != 0) {
                String errorMessage = "Failure";
                if (shellCommand != null) {
                    errorMessage += " in " + shellCommand.toString();
                }
                throw new RuntimeException(errorMessage);
            } else {
                String outputText =  outputStream.toString(ConfigHelper.getStringValue("script.encoding"));
                return outputText;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public String launchStdInOut(String scriptName, String inputMessage) throws RuntimeException {
        String line = ConfigHelper.getStringValue("script.engine")  + " " + ConfigHelper.getStringValue("script.source")+scriptName;
        logger.info(line);
        int exitCode = 0;

        CommandLine mainCmdLine = null;
        try {
            //TODO : use io.vertx.core.cli

            mainCmdLine = CommandLine.parse(line);
            InputStream stdin = new CharSequenceInputStream(inputMessage, 
                Charset.forName(ConfigHelper.getStringValue("script.encoding")));
            ByteArrayOutputStream stderr = new ByteArrayOutputStream();
            ByteArrayOutputStream stdout = new ByteArrayOutputStream();
            PumpStreamHandler streamHandler = new PumpStreamHandler(stdout, stderr, stdin);
            
            DefaultExecutor executor = new DefaultExecutor();
            executor.setStreamHandler(streamHandler);
        
            exitCode = executor.execute(mainCmdLine);
       
            if (exitCode != 0) {
                String errorMessage = "Failure";
                if (mainCmdLine != null) {
                    errorMessage += " in "+mainCmdLine.toString();
                }    
                throw new RuntimeException(errorMessage);    
            }
            else {
                  return stdout.toString(ConfigHelper.getStringValue("script.encoding"));
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }    
    }
}
