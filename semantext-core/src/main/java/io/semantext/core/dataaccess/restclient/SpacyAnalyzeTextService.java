package io.semantext.core.dataaccess.restclient;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import io.semantext.core.domain.text.Text;
import io.semantext.core.domain.text.TextList;

@Path("/")
@RegisterRestClient(configKey = "spacy-api")
@ApplicationScoped
public interface SpacyAnalyzeTextService {

    @POST
    @Path("/entities")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    List entities(Text text);

    @POST
    @Path("/sents")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)    
    TextList sents(Text text);
    
}
