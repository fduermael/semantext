package io.semantext.core.dataaccess.repository;

import io.semantext.core.domain.entity.Entity;
import io.semantext.core.domain.entity.NamedEntity;
import io.semantext.core.domain.text.TextSet;
import io.semantext.core.domain.text.Text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class EntityRepository {
    protected static EntityRepository instance = new EntityRepository();

    // storage per key
    private Map<Text, Collection<Entity>> keyMap = new HashMap<Text, Collection<Entity>>();

    // storage per id
    private Map<String, Entity> idMap = new HashMap<String, Entity>();

    private EntityRepository() {
    }

    public static EntityRepository getInstance() {
        return instance;
    }

    public void add(NamedEntity newConcept) {
        idMap.put(newConcept.getId(), newConcept);
        TextSet name = newConcept.getName();
        if (name != null) {
            for (Text entityKey : name.getSet()) {
                Collection<Entity> entityList = keyMap.getOrDefault(entityKey, new ArrayList<Entity>());
                entityList.add(newConcept);
                keyMap.put(entityKey, entityList);
            }
        }
    }

    public Collection<Entity> findByKey(Text key) {
        return keyMap.get(key);
    }

    public Entity findById(String id) {
        return idMap.get(id);
    }
}
