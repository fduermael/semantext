package io.semantext.core.domain;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import io.semantext.core.domain.base.Metadata;
//import io.semantext.core.domain.base.Metadata.ValueMode;

public class MetadataTest {
    
    @Test
    public void testPutWithSimpleKey() {
        Metadata metadata = new Metadata();
        String key =  "feature";
        String value = "test";
        metadata.put(key, value);
        String storedValue = (String)metadata.get(key);
        Assert.assertEquals(value, storedValue);
    }

    @Test
    public void testPutWithCompositeKey() {
        Metadata metadata = new Metadata();
        String key =  "head/relation";
        String value = "ROOT";
        metadata.put(key, value);
        String storedValue = (String)metadata.get(key);
        Assert.assertEquals(value, storedValue);
    }

    @Test
    public void testPutWithCompositeKeys() {
        Metadata metadata = new Metadata();
        String key1 =  "head/relation";
        String value1 = "ROOT";
        metadata.put(key1, value1);        
        String key2 =  "head/index";
        Integer value2 = 2;
        metadata.put(key2, value2);
        String storedValue1 = (String)metadata.get(key1);
        Assert.assertEquals(value1, storedValue1);
        Integer storedValue2 = (Integer)metadata.get(key2);
        Assert.assertEquals(value2, storedValue2);
    }

    /*

    @Test
    public void testPutWithCompositeKeyMultipleValue() {
        Metadata metadata = new Metadata();
        metadata.setDefaultValueMode(ValueMode.multiple); 
        String key1 =  "head/relation";
        String value1 = "VCOMP";
        metadata.put(key1, value1);        
        String key2 =  "head/relation";
        String value2 = "NCOMP";
        metadata.put(key2, value2);
        List actualValue = (List)metadata.get(key1);
        List expectedValue = new ArrayList<>();
        expectedValue.add(value1);
        expectedValue.add(value2);
        Assert.assertEquals(expectedValue, actualValue);
        
    }

    */
    
    @Test
    public void testPutWithCompositeKeySingleValue() {
        Metadata metadata = new Metadata();
      
        String key1 =  "head/relation";
        String value1 = "VCOMP";
        metadata.put(key1, value1);        
        String key2 =  "head/relation";
        String value2 = "NCOMP";
        metadata.put(key2, value2);
        String actualValue = (String)metadata.get(key1);
       
        Assert.assertEquals(value2, actualValue);
        
    }

    /*
    
    @Test
    public void testPutWithCompositeKeyMultipleValueRegister() {
        Metadata metadata = new Metadata();
        metadata.setDefaultValueMode(ValueMode.single); 
        metadata.register("head/relation",ValueMode.multiple);
        metadata.register("head/score",ValueMode.multiple);
        metadata.register("morphology/category",ValueMode.single);
        String key1 =  "head/relation";
        String value1 = "VCOMP";
        metadata.put(key1, value1);        
        String key2 =  "head/relation";
        String value2 = "NCOMP";
        metadata.put(key2, value2);
        List actualValue = (List)metadata.get(key1);
        List expectedValue = new ArrayList<>();
        expectedValue.add(value1);
        expectedValue.add(value2);
        Assert.assertEquals(expectedValue, actualValue);
        
    }
*/
}
