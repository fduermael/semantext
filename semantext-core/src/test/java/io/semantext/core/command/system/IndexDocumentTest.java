package io.semantext.core.command.system;

import io.quarkus.test.junit.QuarkusTest;

import io.semantext.core.domain.document.Document;
import io.semantext.core.command.document.*;
import io.semantext.core.command.file.SearchIndexDocumentCommand;
import io.semantext.core.command.system.AddIndexDocumentCommand;
import io.semantext.core.domain.file.File;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@QuarkusTest
public class IndexDocumentTest {

    static Logger logger = LoggerFactory.getLogger(IndexDocumentTest.class);

    @Test
    public void testIndexDocument() {

        ExtractDocumentMetadataCommand extractDocumentMetadataCommand = new ExtractDocumentMetadataCommand();

        AddIndexDocumentCommand indexDocumentCommand = new AddIndexDocumentCommand();
        SearchIndexDocumentCommand findDocumentCommand = new SearchIndexDocumentCommand();

        Document file = new Document("C:/_DVT/datasets/semantext/images/","fleur.jpg");
        
           
        Document document = extractDocumentMetadataCommand .executeUnit(file);
        indexDocumentCommand.executeUnit(document);

        List<File> fileList = findDocumentCommand.executeExpand("fleur");

        System.out.println(fileList);
       
    }

}
