package io.semantext.core.command.text;

import io.semantext.core.command.text.StanfordCoreNLPAnalyzeTextCommand;
import io.semantext.core.domain.base.TreeNode;
import io.semantext.core.domain.base.TreeNodeFormatterVisitor;
import io.semantext.core.domain.text.Text;
import io.semantext.core.factory.text.StanfordCoreNLPAnalyzerFactory;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

//@QuarkusTest
public class StanfordCoreNLPAnalyzeTextCommandTest {

    static Logger logger = LoggerFactory.getLogger(StanfordCoreNLPAnalyzeTextCommandTest.class);

    //@BeforeAll
    public void initAll() {
        System.out.println("---init all---");
        StanfordCoreNLPAnalyzerFactory.initAll();
    }


    @Test
    public void testCoreNlpFrench() {
        String input = "J'ai mangé, il y a deux mois, des pommes de terre cuites à la graisse de magret de canard dans un restaurant de spécialités du Sud-Ouest à Paris. Elles étaient très bonnes.";
        StanfordCoreNLP pipeline = StanfordCoreNLPAnalyzerFactory.getInstance(Locale.FRENCH);
        CoreDocument doc = pipeline.processToCoreDocument(input);
        logger.info("--sentences--");
        doc.sentences().stream().forEach(x->logger.info(x.toString()));
        logger.info("--tokens--");
        doc.tokens().stream().forEach(x->logger.info(x.toString()));
                 
    }

    @Test
    public void testCoreNlpAnnotationFrench() {
        //String input = "J'ai mangé des pommes de terre. Elles étaient très bonnes.";
        String input = "J'ai mangé, il y a deux mois, des pommes de terre cuites à la graisse de magret de canard dans un restaurant de spécialités du Sud-Ouest à Paris. Elles étaient très bonnes.";
        Text paragraph = new Text(input);

        
        StanfordCoreNLPAnalyzeTextCommand command = new StanfordCoreNLPAnalyzeTextCommand();
        command.executeUnit(paragraph);
        logger.info("--paragraph--");
        logger.info(paragraph.toString());
    }

    @Test
    public void testCoreNlpEnglish() {
        String input = "Do you want to call me back? No, thanks!";
        StanfordCoreNLP pipeline = StanfordCoreNLPAnalyzerFactory.getInstance(Locale.ENGLISH);
        CoreDocument doc = pipeline.processToCoreDocument(input);
        logger.info("--sentences--");
        doc.sentences().stream().forEach(x->logger.info(x.toString()));
        logger.info("--tokens--");
        doc.tokens().stream().forEach(x->logger.info(x.toString()));
         
    }

    
    @Test
    public void testCoreNlpAnnotationEnglish() {
        //String input = "Do you want to call me back? No, thanks!";
        String input ="Two months ago, I ate potatoes cooked with duck breast fat in a South-West specialty restaurant in Paris. They were very good.";
        Text paragraph = new Text("en",input);
        StanfordCoreNLPAnalyzeTextCommand command = new StanfordCoreNLPAnalyzeTextCommand();
        command.executeUnit(paragraph);
        logger.info("--paragraph--");
        logger.info(paragraph.toString());

        paragraph.getComponentList().forEach(sentence -> {
            
            List<String> rootNameList = sentence.findComponentsByMetadataPath("dep_head_relation", "ROOT")
            .stream()
            .map (root-> root.getName())
            .collect(Collectors.toList());

            System.out.println(rootNameList);

            List<TreeNode<Text>> treeNodeTextList = sentence.makeDependencyTreeList();

            TreeNodeFormatterVisitor formatter = new TreeNodeFormatterVisitor();

            treeNodeTextList.forEach( treeNodeText -> { 
               treeNodeText.accept(formatter);
            });

        });
    }
}
