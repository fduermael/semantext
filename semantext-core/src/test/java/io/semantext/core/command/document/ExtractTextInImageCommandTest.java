package io.semantext.core.command.document;

//import io.quarkus.test.junit.QuarkusTest;
import io.semantext.core.domain.image.Image;
import io.semantext.core.command.image.AnnotateTextInImageCommand;
import io.semantext.core.domain.file.File;

import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

//@QuarkusTest
public class ExtractTextInImageCommandTest {

    static Logger logger = LoggerFactory.getLogger(ExtractTextInImageCommandTest.class);

    @Test
    public void testRecognizeText() {

        AnnotateTextInImageCommand command = new AnnotateTextInImageCommand();
        File file = new File("c:/_DVT/datasets/semantext/images/","Objectifs_formation_DevOps.JPG");
        Image image = command.executeUnit(file);
        
        image.getAnnotationList().forEach(a->logger.info(a.getText().getRaw()));

        System.out.println(image.getAnnotationList().get(0).getText().getRaw());
    }

}
