package io.semantext.core.command.text;

import io.quarkus.test.junit.QuarkusTest;
import io.semantext.core.command.text.SpacyAnalyzeTextCommand;
import io.semantext.core.domain.base.TreeNode;
import io.semantext.core.domain.base.TreeNodeFormatterVisitor;
import io.semantext.core.domain.text.Text;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;


@QuarkusTest
public class SpacyAnalyzeTextCommandTest {

    static Logger logger = Logger.getLogger(SpacyAnalyzeTextCommandTest.class.getName());

    @Test
    public void testRecognizeText() {

        SpacyAnalyzeTextCommand command = new SpacyAnalyzeTextCommand();
        Text paragraph = new Text("fr", "Il faut que tu envoies les lettres de mon père à sa soeur. Oui !");
        
        paragraph = command.executeUnit(paragraph);


        paragraph.getComponentList().forEach(sentence -> {
            
            List<String> rootNameList = sentence.findComponentsByMetadataPath("dep_head_relation", "ROOT")
            .stream()
            .map (root-> root.getName())
            .collect(Collectors.toList());

            System.out.println(rootNameList);

            List<TreeNode<Text>> treeNodeTextList = sentence.makeDependencyTreeList();

            TreeNodeFormatterVisitor formatter = new TreeNodeFormatterVisitor();

            treeNodeTextList.forEach( treeNodeText -> { 
               treeNodeText.accept(formatter);
            });

        });
    }    
    

    @Test
    public void testRecognizeText2() {

        SpacyAnalyzeTextCommand command = new SpacyAnalyzeTextCommand();
        Text paragraph = new Text("en","Two months ago, I ate potatoes cooked with duck breast fat in a South-West specialty restaurant in Paris. They were very good.");

        paragraph = command.executeUnit(paragraph);


        paragraph.getComponentList().forEach(sentence -> {
            
            List<String> rootNameList = sentence.findComponentsByMetadataPath("dep_head_relation", "ROOT")
            .stream()
            .map (root-> root.getName())
            .collect(Collectors.toList());

            System.out.println(rootNameList);

            List<TreeNode<Text>> treeNodeTextList = sentence.makeDependencyTreeList();

            TreeNodeFormatterVisitor formatter = new TreeNodeFormatterVisitor();

            treeNodeTextList.forEach( treeNodeText -> { 
               treeNodeText.accept(formatter);
            });

        });
    }    

}