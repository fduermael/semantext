package io.semantext.core.command.image;

import io.semantext.core.command.image.ScanImageCommand;
import io.semantext.core.domain.image.Image;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

//@QuarkusTest
public class ScanImageCommandTest {

    static Logger logger = LoggerFactory.getLogger(ScanImageCommandTest.class);

    @Test
    public void testScanText() {

        ScanImageCommand command = new ScanImageCommand();
        Image image = command.executeUnit(null);
        Assert.assertNotNull(image);
        logger.info("Size:"+image.getRaw().length);
    }

}
