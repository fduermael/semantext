package io.semantext.core.command.text;

import io.semantext.core.command.text.CleanTextCommand;
import io.semantext.core.domain.text.Text;

import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

//@QuarkusTest
public class CleanTextCommandTest {

    static Logger logger = LoggerFactory.getLogger(CleanTextCommandTest.class);

    @Test
    public void testRecognizeText() {

        CleanTextCommand command = new CleanTextCommand();
        Text text = new Text("TESTé");
        text.setLang("fr");

        text = command.executeUnit(text);

        System.out.println(text);
    }

}