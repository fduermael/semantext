package io.semantext.core.command.system;

import io.quarkus.test.junit.QuarkusTest;

import io.semantext.core.domain.document.Document;
import io.semantext.core.command.document.*;
import io.semantext.core.command.file.SearchIndexDocumentCommand;
import io.semantext.core.domain.file.File;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@QuarkusTest
public class SearchIndexDocumentTest {

    static Logger logger = LoggerFactory.getLogger(SearchIndexDocumentTest.class);

    @Test
    public void testIndexDocument() {
        SearchIndexDocumentCommand findDocumentCommand = new SearchIndexDocumentCommand();
    
       
        List<File> fileList = findDocumentCommand.executeExpand("fleur");

        System.out.println(fileList);
       
    }

}
