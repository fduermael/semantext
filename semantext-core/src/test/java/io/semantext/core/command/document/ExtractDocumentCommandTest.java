package io.semantext.core.command.document;

import io.semantext.core.domain.file.File;
import io.semantext.core.command.document.ExtractDocumentMetadataAndContentCommand;
import io.semantext.core.domain.document.Document;

import java.io.FileInputStream;

import java.io.InputStream;

import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

//@QuarkusTest
public class ExtractDocumentCommandTest {

    static Logger logger = LoggerFactory.getLogger(ExtractTextInImageCommandTest.class);

    @Test
    public void testRecognizeText() {

        ExtractDocumentMetadataAndContentCommand command = new ExtractDocumentMetadataAndContentCommand();
        //InputStream stream = this.getClass().getClassLoader()
        //.getResourceAsStream("src/test/resources/Objectifs_formation_DevOps.JPG");
        File file = new File("C:/_DVT/datasets/semantext/images/","Objectifs_formation_DevOps.JPG");
        
           
        Document document = command.executeUnit(file);

        System.out.println(document);
        System.out.println(document.getSectionList().get(0));

       
    }

    @Test
    public void testRecognizeTextInPDF() {

        ExtractDocumentMetadataAndContentCommand command = new ExtractDocumentMetadataAndContentCommand();
        //InputStream stream = this.getClass().getClassLoader()
        //.getResourceAsStream("src/test/resources/Objectifs_formation_DevOps.JPG");
        
            //InputStream stream = new FileInputStream(new java.io.File("C:/_DVT/datasets/semantext/images/Carte_Identite_FDuermael_20100917.pdf"));
        
        File file = new File("C:/_DVT/datasets/semantext/images/","Tablet_Lenovo_Invoice.pdf");
        
        Document document = command.executeUnit(file);

        System.out.println(document);
        if (document.getSectionList().size()>0) {
            System.out.println(document.getSectionList().get(0));
        }

    }

}