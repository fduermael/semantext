package io.semantext.core.command.note;

import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.credentials.CredentialsProvider;
import io.quarkus.test.junit.QuarkusTest;
import io.semantext.core.domain.note.NoteCollectionMap;
import io.semantext.semantext.cli.config.PropertiesCredentialProvider;
@QuarkusTest
public class ExtractNoteInGoogleKeepTest {


        static Logger logger = LoggerFactory.getLogger(ExtractNoteInGoogleKeepTest.class);

        //@Inject
        CredentialsProvider credentialsProvider = new PropertiesCredentialProvider();

    
        @Test
        public void testExtractNote() {
            ExtractNoteInGoogleKeepCommand extractNoteInGoogleKeepCommand = new ExtractNoteInGoogleKeepCommand();
        
            //TODO https://quarkus.io/guides/credentials-provider

            
            Map<String,Object> parameterMap = new HashMap<String,Object>(credentialsProvider.getCredentials("google"));
        

            extractNoteInGoogleKeepCommand.setParameterMap(parameterMap);
            
            List<String> labelList = Arrays.asList("Loisirs");

           NoteCollectionMap noteCollectionMap = extractNoteInGoogleKeepCommand.executeUnit(labelList);
         

           assertFalse(noteCollectionMap.isEmpty());
           
        }
    
    }

