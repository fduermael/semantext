package io.semantext.semantext.cli;

import java.io.*;

import javax.inject.Inject;

import io.semantext.core.domain.document.Document;
import io.semantext.core.domain.document.Page;
import io.semantext.core.domain.file.File;
import io.semantext.core.domain.image.Image;
import io.semantext.core.domain.text.FramedText;
import io.semantext.core.service.PdfFileExtractionService;
import picocli.CommandLine;

@CommandLine.Command(name = "DocumentExtraction", description = "Extract texts and images")
public class DocumentExtractionCli implements Runnable {
    
    @Inject
    PdfFileExtractionService pdfFileExtractionService;

    @CommandLine.Option(names = {"-f", "--fileName"}, description = "Input file name")
    String fileName;

    @CommandLine.Option(names = {"-o", "--outputPath"}, description = "Output path of folder")
    String outputPath;

    @Override
    public void run()  {

        File file = new File(fileName);
        Document document = pdfFileExtractionService.content(file);

        new java.io.File(outputPath).mkdirs();
        int pageNumber=0;
        for (Page page : document.getPageList()) {
            pageNumber++;
            String folderName = outputPath + "/" + "page" + pageNumber;
            new java.io.File(folderName).mkdirs();
            int textNumber=0;
            for (FramedText framedText : page.getFramedTextList() ) {
                textNumber++;
                java.io.File textFile = new java.io.File(folderName, "text"+textNumber+".txt");
                try (OutputStream outputStream = new FileOutputStream(textFile)) {
                   outputStream.write(framedText.getText().getRaw().getBytes());
                }
                catch (IOException e) {
                    continue;
                }
            }
            int imageNumber=0;
            for (Image image : page.getImageList() ) {
                imageNumber++;
                java.io.File imageFile = new java.io.File(folderName, "image"+imageNumber+".png");
                try (OutputStream outputStream = new FileOutputStream(imageFile)) {
                   outputStream.write(image.getRaw());
                }
                catch (IOException e) {
                    continue;
                }
            }
        }
    }

}
