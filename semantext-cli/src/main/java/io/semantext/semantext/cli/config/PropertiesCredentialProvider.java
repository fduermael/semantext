package io.semantext.semantext.cli.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.config.Config;

import io.quarkus.credentials.CredentialsProvider;

import io.quarkus.arc.Unremovable;

@ApplicationScoped
@Unremovable
public class PropertiesCredentialProvider implements CredentialsProvider {


    @Inject
    Config config;

    @Override
	public Map<String, String> getCredentials(String credentialsProviderName) {
		Map<String, String> properties = new HashMap<>();
        
        String credentialsPathname = "/home/frup43047/credentials.properties";
        
        if (config!=null) {
            credentialsPathname=config.getValue("credentials.dir", String.class);
        }


        Properties credentialsProperties = new Properties();
        try {
            credentialsProperties.load(new FileInputStream(credentialsPathname));
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }

		properties.put(USER_PROPERTY_NAME, credentialsProperties.getProperty(credentialsProviderName+"."+USER_PROPERTY_NAME));
		properties.put(PASSWORD_PROPERTY_NAME, credentialsProperties.getProperty(credentialsProviderName+"."+PASSWORD_PROPERTY_NAME));
		return properties;
	}
 
}
